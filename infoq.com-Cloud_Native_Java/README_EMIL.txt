https://github.com/joshlong/bootiful-microservices-config


---------- config-service (8888) ---------------
Incepi prin a porni CONFIG intai si apoi celelalte proiecte.
ConfigServiceApplication

http://localhost:8888/health
http://localhost:8888/metrics
cinfiguration links are below for all the applications.

---------- eureka-service (8761) ---------------
EurekaServiceApplication
# the server for registering available live instances.
# Used by REST clients apps when calling REST server apps
# like reservation-client -> reservation-service

		# configuration of this server in the configuration server.
http://localhost:8888/eureka-service/default

http://localhost:8761

---------- reservation-service (8000) ---------------
ReservationServiceApplication
http://localhost:8000/message
		# configuration of this server in the configuration server.
http://localhost:8888/reservation-service/default

# http://localhost:8000/refresh
http://localhost:8000/
http://localhost:8000/reservations
http://localhost:8000/message

http://localhost:8000/metrics
http://localhost:8000/health

---------- reservation-client (9999) ---------------
		# configuration of this server in the configuration server.
http://localhost:8888/reservation-client/default

http://localhost:9999/health
http://localhost:9999/hystrix
http://localhost:9999/hystrix.stream

# simply proxy "reservation-service" (ReservationReader).
# see @EnableZuulProxy
http://localhost:9999/reservation-service/reservations
http://localhost:9999/reservation-service/message

# the new rest api.
# See @EnableFeignClients
http://localhost:9999/reservations/names



=====================
@EnableDiscoveryClient - activates the registering to the EUREKA app.

@EnableFeignClients - allows us to use @FeignClient. To call other controllers in java even though it is a REST call behind the scene.

@EnableZuulProxy
+ eureka
= you have proxy for the rest of all applications in the eureka.
e.g.:
http://localhost:9999/reservation-service/message

@EnableCircuitBreaker
+
@HystrixCommand(fallbackMethod = "fallback")
= if the remote service is down or timeout it will return the values from the fallback method.


