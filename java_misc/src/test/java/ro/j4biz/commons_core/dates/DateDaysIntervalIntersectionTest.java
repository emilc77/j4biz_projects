package ro.j4biz.commons_core.dates;

import static com.google.common.base.Preconditions.checkState;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Test;

public class DateDaysIntervalIntersectionTest {
  private int year = 2015;
  private int month = 4;

  @Test
  public void testOverlap1() throws ParseException {
    DaysIntervalIntersection intervalIntersection = new DaysIntervalIntersection(getDate(1), getDate(3), getDate(2),
        getDate(4));
    List<Date> intersection = intervalIntersection.getIntersectionAsDates();
    assertEquals(2, intersection.size());
    assertEqualDates(getDate(2), intersection.get(0));
    assertEqualDates(getDate(3), intersection.get(1));
  }

  @Test
  public void testOverlap2() throws ParseException {
    DaysIntervalIntersection intervalIntersection = new DaysIntervalIntersection(getDate(1), getDate(10), getDate(2),
        getDate(4));
    List<Date> intersection = intervalIntersection.getIntersectionAsDates();
    assertEquals(3, intersection.size());
    assertEqualDates(getDate(2), intersection.get(0));
    assertEqualDates(getDate(3), intersection.get(1));
    assertEqualDates(getDate(4), intersection.get(2));
  }

  @Test
  public void testAbut1() throws ParseException {
    DaysIntervalIntersection intervalIntersection = new DaysIntervalIntersection(getDate(1), getDate(2), getDate(2),
        getDate(4));
    List<Date> intersection = intervalIntersection.getIntersectionAsDates();
    assertEquals(1, intersection.size());
    assertEqualDates(getDate(2), intersection.get(0));
  }

  @Test
  public void testAbut2() throws ParseException {
    DaysIntervalIntersection intervalIntersection = new DaysIntervalIntersection(getDate(4), getDate(7), getDate(2),
        getDate(4));
    List<Date> intersection = intervalIntersection.getIntersectionAsDates();
    assertEquals(1, intersection.size());
    assertEqualDates(getDate(4), intersection.get(0));
  }

  @Test
  public void testAbutWithTime1() throws ParseException {
    DaysIntervalIntersection intervalIntersection = new DaysIntervalIntersection(getDate(1), getDate(2, 10), getDate(2,
        12), getDate(4));
    List<Date> intersection = intervalIntersection.getIntersectionAsDates();
    assertEquals(1, intersection.size());
    assertEqualDates(getDate(2), intersection.get(0));
  }

  @Test
  public void testWithGap1() throws ParseException {
    DaysIntervalIntersection intervalIntersection = new DaysIntervalIntersection(getDate(1), getDate(2), getDate(3),
        getDate(4));
    List<Date> intersection = intervalIntersection.getIntersectionAsDates();
    assertEquals(0, intersection.size());
  }

  @Test
  public void testWithGap2() throws ParseException {
    DaysIntervalIntersection intervalIntersection = new DaysIntervalIntersection(getDate(3), getDate(4), getDate(1),
        getDate(2));
    List<Date> intersection = intervalIntersection.getIntersectionAsDates();
    assertEquals(0, intersection.size());
  }

  @Test
  public void testErrorIncorrectIntervals1() throws ParseException {
    try {
      new DaysIntervalIntersection(getDate(2), getDate(1), getDate(3), getDate(4));
      fail();
    } catch (IllegalArgumentException ignored) {
    }
  }

  @Test
  public void testErrorIncorrectIntervals2() throws ParseException {
    try {
      new DaysIntervalIntersection(getDate(1), getDate(2), getDate(6), getDate(4));
      fail();
    } catch (IllegalArgumentException ignored) {
    }
  }

  @Test
  public void testNull1() throws ParseException {
    try {
      new DaysIntervalIntersection(null, getDate(1), getDate(3), getDate(4));
      fail();
    } catch (NullPointerException ignored) {
    }
  }

  @Test
  public void testNull2() throws ParseException {
    try {
      new DaysIntervalIntersection(getDate(1), null, getDate(3), getDate(4));
      fail();
    } catch (NullPointerException ignored) {
    }
  }

  @Test
  public void testNull3() throws ParseException {
    try {
      new DaysIntervalIntersection(getDate(1), getDate(3), null, getDate(4));
      fail();
    } catch (NullPointerException ignored) {
    }
  }

  @Test
  public void testNull4() throws ParseException {
    try {
      new DaysIntervalIntersection(getDate(1), getDate(3), getDate(4), null);
      fail();
    } catch (NullPointerException ignored) {
    }
  }

  private Date getDate(int day) {
    DateTime dateTime = new DateTime(year, month, day, 0, 0, 0, 0);
    return dateTime.toDate();
  }

  private Date getDate(int day, int hour) {
    DateTime dateTime = new DateTime(year, month, day, hour, 0, 0, 0);
    return dateTime.toDate();
  }

  private void assertEqualDates(Date date1, Date date2) {
    if (date1 == null && date2 == null) {
      return;
    }

    checkState(date1.getTime() == date2.getTime(), "Different dates '%s' != '%s'", date1, date2);
  }

}
