package ro.j4biz.csv_parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.io.ByteStreams;

public class MyCsvParserTest {

  @Test
  public void testOverlap1() throws IOException {
    InputStream inStream = MyCsvParserTest.class.getResourceAsStream("/ro/j4biz/csv_parser/test_excel.csv");
    byte[] bytes = ByteStreams.toByteArray(inStream);

    MyCsvParser parser = new MyCsvParser(bytes);
    Map<String, CsvData> mapData = parser.getData();

    String[] expectedheader = { "COL 1", " COL 2 ", "Col 3" };
    {
      CsvData data = mapData.get("D23");
      Assert.assertNotNull(data);

      String[] header = data.getHeader();
      Arrays.equals(expectedheader, header);
    }

    {
      CsvData data = mapData.get("M33");
      Assert.assertNotNull(data);

      String[] header = data.getHeader();
      Arrays.equals(expectedheader, header);
    }

  }

}
