package ro.j4biz;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class WildcardsTest {

  @Test
  public void testStar() {
    // List<String> values = Arrays
    // .asList();

    Wildcards wildcards = new Wildcards("te*t");

    assertTrue(wildcards.matchesOneLine("test"));
    assertTrue(wildcards.matchesOneLine("   test   "));
    assertTrue(wildcards.matchesOneLine("   XXXtest   "));
    assertTrue(wildcards.matchesOneLine("<>   test   <>"));
    assertTrue(wildcards.matchesOneLine("<>   teXXXt   <>"));
    assertTrue(wildcards.matchesOneLine("testy"));

    assertFalse(wildcards.matchesOneLine("<>   teXXXt \n\n  <>"));
    assertTrue(wildcards.matchesMultipleLine("<>   teXXXt \n\n  <>"));

    assertTrue(wildcards.matchesOneLine("testy"));
    assertTrue(wildcards.matchesMultipleLine("testy"));
    assertTrue(wildcards.matchesOneLine("tether"));
    assertTrue(wildcards.matchesOneLine("teat"));
    assertTrue(wildcards.matchesOneLine("tempest"));

    assertFalse(wildcards.matchesOneLine("best"));
    assertFalse(wildcards.matchesOneLine("   best   "));
    assertFalse(wildcards.matchesOneLine("   XXXbest   "));
    assertFalse(wildcards.matchesOneLine("<>   best   <>"));
    assertFalse(wildcards.matchesOneLine("<>   beXXXt   <>"));

    assertFalse(wildcards.matchesOneLine("best"));
    assertFalse(wildcards.matchesOneLine("crest"));
    assertFalse(wildcards.matchesOneLine("zest"));
    assertFalse(wildcards.matchesOneLine("temper"));
  }

  @Test
  public void testQuestion() {
    // List<String> values = Arrays
    // .asList();

    Wildcards wildcards = new Wildcards("te?t");

    assertTrue(wildcards.matchesOneLine("test"));
    assertTrue(wildcards.matchesOneLine("   test   "));
    assertTrue(wildcards.matchesOneLine("   XXXtest   "));
    assertTrue(wildcards.matchesOneLine("<>   test   <>"));
    assertFalse(wildcards.matchesOneLine("<>   teXXXt   <>"));
    assertTrue(wildcards.matchesOneLine("testy"));

    assertFalse(wildcards.matchesOneLine("<>   teXXXt \n\n  <>"));
    assertFalse(wildcards.matchesMultipleLine("<>   teXXXt \n\n  <>"));

    assertTrue(wildcards.matchesOneLine("testy"));
    assertTrue(wildcards.matchesMultipleLine("testy"));
    assertFalse(wildcards.matchesOneLine("tether"));
    assertTrue(wildcards.matchesOneLine("teat"));
    assertFalse(wildcards.matchesOneLine("tempest"));

    assertFalse(wildcards.matchesOneLine("best"));
    assertFalse(wildcards.matchesOneLine("   best   "));
    assertFalse(wildcards.matchesOneLine("   XXXbest   "));
    assertFalse(wildcards.matchesOneLine("<>   best   <>"));
    assertFalse(wildcards.matchesOneLine("<>   beXXXt   <>"));

    assertFalse(wildcards.matchesOneLine("best"));
    assertFalse(wildcards.matchesOneLine("crest"));
    assertFalse(wildcards.matchesOneLine("zest"));
    assertFalse(wildcards.matchesOneLine("temper"));
  }

  @Test
  public void testCombined1() {
    // List<String> values = Arrays
    // .asList();

    Wildcards wildcards = new Wildcards("te*?t");

    assertTrue(wildcards.matchesOneLine("test"));
    assertTrue(wildcards.matchesOneLine("   test   "));
    assertTrue(wildcards.matchesOneLine("   XXXtest   "));
    assertTrue(wildcards.matchesOneLine("<>   test   <>"));
    assertTrue(wildcards.matchesOneLine("<>   teXXXt   <>"));
    assertTrue(wildcards.matchesOneLine("testy"));

    assertFalse(wildcards.matchesOneLine("<>   teXXXt \n\n  <>"));
    assertTrue(wildcards.matchesMultipleLine("<>   teXXXt \n\n  <>"));

    assertTrue(wildcards.matchesOneLine("testy"));
    assertTrue(wildcards.matchesMultipleLine("testy"));
    assertFalse(wildcards.matchesOneLine("tether"));
    assertTrue(wildcards.matchesOneLine("teat"));
    assertTrue(wildcards.matchesOneLine("tempest"));

    assertFalse(wildcards.matchesOneLine("best"));
    assertFalse(wildcards.matchesOneLine("   best   "));
    assertFalse(wildcards.matchesOneLine("   XXXbest   "));
    assertFalse(wildcards.matchesOneLine("<>   best   <>"));
    assertFalse(wildcards.matchesOneLine("<>   beXXXt   <>"));

    assertFalse(wildcards.matchesOneLine("best"));
    assertFalse(wildcards.matchesOneLine("crest"));
    assertFalse(wildcards.matchesOneLine("zest"));
    assertFalse(wildcards.matchesOneLine("temper"));
  }

  @Test
  public void testCombined2() {
    // List<String> values = Arrays
    // .asList();

    Wildcards wildcards = new Wildcards("te?*t");

    assertTrue(wildcards.matchesOneLine("test"));
    assertTrue(wildcards.matchesOneLine("   test   "));
    assertTrue(wildcards.matchesOneLine("   XXXtest   "));
    assertTrue(wildcards.matchesOneLine("<>   test   <>"));
    assertTrue(wildcards.matchesOneLine("<>   teXXXt   <>"));
    assertTrue(wildcards.matchesOneLine("testy"));

    assertFalse(wildcards.matchesOneLine("<>   teXXXt \n\n  <>"));
    assertTrue(wildcards.matchesMultipleLine("<>   teXXXt \n\n  <>"));

    assertTrue(wildcards.matchesOneLine("testy"));
    assertTrue(wildcards.matchesMultipleLine("testy"));
    assertFalse(wildcards.matchesOneLine("tether"));
    assertTrue(wildcards.matchesOneLine("teat"));
    assertTrue(wildcards.matchesOneLine("tempest"));

    assertFalse(wildcards.matchesOneLine("best"));
    assertFalse(wildcards.matchesOneLine("   best   "));
    assertFalse(wildcards.matchesOneLine("   XXXbest   "));
    assertFalse(wildcards.matchesOneLine("<>   best   <>"));
    assertFalse(wildcards.matchesOneLine("<>   beXXXt   <>"));

    assertFalse(wildcards.matchesOneLine("best"));
    assertFalse(wildcards.matchesOneLine("crest"));
    assertFalse(wildcards.matchesOneLine("zest"));
    assertFalse(wildcards.matchesOneLine("temper"));
  }

}
