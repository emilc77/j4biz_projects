package ro.j4biz.cache_date_vat_percent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static ro.j4biz.DateTstUtils.getDate;

import java.text.ParseException;

import org.junit.Test;

public class VatPercentFinderTest {

  @Test
  public void testNoInterval() throws ParseException {
    try {
      VatPercentFinder finder = new VatPercentFinder();
      finder.getVatPercent(getDate(10));
      fail();
    } catch (RuntimeException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testBeforeStart() throws ParseException {
    try {
      VatPercentFinder finder = new VatPercentFinder();
      finder.addInterval(getDate(10), null, 24);
      finder.getVatPercent(getDate(9));
      fail();
    } catch (RuntimeException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testAfterEndStart() throws ParseException {
    try {
      VatPercentFinder finder = new VatPercentFinder();
      finder.addInterval(getDate(10), getDate(15), 24);
      finder.getVatPercent(getDate(19));
      fail();
    } catch (RuntimeException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testDuplicateValue() throws ParseException {
    try {
      VatPercentFinder finder = new VatPercentFinder();
      finder.addInterval(getDate(10), getDate(15), 24);
      finder.addInterval(getDate(10), null, 24);
      finder.getVatPercent(getDate(12));
      fail();
    } catch (RuntimeException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testOk() throws ParseException {
    VatPercentFinder finder = new VatPercentFinder();
    finder.addInterval(getDate(10), getDate(15), 24);
    finder.addInterval(getDate(15), null, 9);
    assertEquals(24, finder.getVatPercent(getDate(10)), 0.01);
    assertEquals(24, finder.getVatPercent(getDate(14)), 0.01);
    assertEquals(9, finder.getVatPercent(getDate(15)), 0.01);
    assertEquals(9, finder.getVatPercent(getDate(16)), 0.01);
  }

}
