package ro.j4biz.cache_date_vat_percent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static ro.j4biz.DateTstUtils.getDate;

import java.text.ParseException;
import java.util.Date;

import org.junit.Test;

public class VatIntervalTest {
  @Test
  public void testStartNull() throws ParseException {
    try {
      VatInterval interval = new VatInterval(null, new Date(), 0.2323);
      fail();
    } catch (Exception e) {
      // ignored
    }
    assertEquals(2, 2);
  }

  @Test
  public void testNegativePercentage() throws ParseException {
    try {
      VatInterval interval = new VatInterval(new Date(), new Date(), -0.2323);
      fail();
    } catch (Exception e) {
      // ignored
    }
  }

  @Test
  public void testEndNull() throws ParseException {
    VatInterval interval = new VatInterval(getDate(10), null, 0.2323);
    assertFalse(interval.matchesInterval(getDate(9)));
    assertTrue(interval.matchesInterval(getDate(10)));
    assertTrue(interval.matchesInterval(getDate(11)));
  }

  @Test
  public void testInvalidInterval() throws ParseException {
    try {
      VatInterval interval = new VatInterval(getDate(10), getDate(10), 23);
      fail();
    } catch (Exception e) {
      // ignored
    }
    try {
      VatInterval interval = new VatInterval(getDate(11), getDate(10), 23);
      fail();
    } catch (Exception e) {
      // ignored
    }
  }

  @Test
  public void testEndNotNull() throws ParseException {
    VatInterval interval = new VatInterval(getDate(10), getDate(15), 0.2323);
    assertFalse(interval.matchesInterval(getDate(9)));
    assertTrue(interval.matchesInterval(getDate(10)));
    assertTrue(interval.matchesInterval(getDate(11)));
    assertFalse(interval.matchesInterval(getDate(15)));
    assertFalse(interval.matchesInterval(getDate(16)));
  }

}
