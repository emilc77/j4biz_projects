package ro.j4biz;

import org.junit.Test;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;

public class ExcelColumnTranslatorTest {

    @Test
    public void testOverlap1() throws IOException {
        assertEquals("A", t(0));
        assertEquals("Z", t(25));

        for (int i = 0; i < ExcelColumnTranslator.LETTERS_USED.length; i++) {
            char letter = ExcelColumnTranslator.LETTERS_USED[i];
            StringBuilder b = new StringBuilder();
            b.append(letter);
            assertEquals(b.toString(), t(i));
        }

        assertEquals("AA", t(26 * 1 + 0));
        assertEquals("AZ", t(26 * 1 + 25));
        assertEquals("BA", t(26 * 2 + 0));
        assertEquals("BZ", t(26 * 2 + 25));
        assertEquals("CA", t(26 * 3 + 0));
        assertEquals("CZ", t(26 * 3 + 25));

        assertEquals("YA", t(26 * 25 + 0));
        assertEquals("YZ", t(26 * 25 + 25));
        assertEquals("ZA", t(26 * 26 + 0));
        assertEquals("ZZ", t(26 * 26 + 25));

        assertEquals("AAA", t(26 * 26 * 1 + 26 * 1 + 0));
        assertEquals("AAZ", t(26 * 26 * 1 + 26 * 1 + 25));
    }

    private String t(int columnIndex) {
        return ExcelColumnTranslator.translateNumericToString(columnIndex);
    }

}
