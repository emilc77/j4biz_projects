package ro.j4biz.nationala_info_2015.cl_09_cubul;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CubeSpreadCalculatorTest {

  @Test
  public void test3() {
    CubeSpreadCalculator calculator = new CubeSpreadCalculator(3);
    calculator.computeSpread();
    int[][][] spreadOfNumbers = calculator.getSpreadOfNumbers();

    assertEquals(1, spreadOfNumbers[0][0][0]);
    assertEquals(2, spreadOfNumbers[0][1][0]);
    assertEquals(3, spreadOfNumbers[0][2][0]);
    assertEquals(4, spreadOfNumbers[0][2][1]);
    assertEquals(5, spreadOfNumbers[0][2][2]);
    assertEquals(6, spreadOfNumbers[0][1][2]);
    assertEquals(7, spreadOfNumbers[0][0][2]);
    assertEquals(8, spreadOfNumbers[0][0][1]);
    assertEquals(9, spreadOfNumbers[0][1][1]);

    assertEquals(10, spreadOfNumbers[1][1][1]);
    assertEquals(11, spreadOfNumbers[1][0][1]);
    assertEquals(12, spreadOfNumbers[1][0][2]);
    assertEquals(13, spreadOfNumbers[1][1][2]);
    assertEquals(14, spreadOfNumbers[1][2][2]);
    assertEquals(15, spreadOfNumbers[1][2][1]);
    assertEquals(16, spreadOfNumbers[1][2][0]);
    assertEquals(17, spreadOfNumbers[1][1][0]);
    assertEquals(18, spreadOfNumbers[1][0][0]);

    assertEquals(19, spreadOfNumbers[2][0][0]);
    assertEquals(20, spreadOfNumbers[2][1][0]);
    assertEquals(21, spreadOfNumbers[2][2][0]);
    assertEquals(22, spreadOfNumbers[2][2][1]);
    assertEquals(23, spreadOfNumbers[2][2][2]);
    assertEquals(24, spreadOfNumbers[2][1][2]);
    assertEquals(25, spreadOfNumbers[2][0][2]);
    assertEquals(26, spreadOfNumbers[2][0][1]);
    assertEquals(27, spreadOfNumbers[2][1][1]);
  }

  @Test
  public void test4() {
    CubeSpreadCalculator calculator = new CubeSpreadCalculator(4);
    calculator.computeSpread();
    int[][][] spreadOfNumbers = calculator.getSpreadOfNumbers();

    int i = 1;

    assertEquals(i++, spreadOfNumbers[0][0][0]);
    assertEquals(i++, spreadOfNumbers[0][1][0]);
    assertEquals(i++, spreadOfNumbers[0][2][0]);
    assertEquals(i++, spreadOfNumbers[0][3][0]);

    assertEquals(i++, spreadOfNumbers[0][3][1]);
    assertEquals(i++, spreadOfNumbers[0][3][2]);
    assertEquals(i++, spreadOfNumbers[0][3][3]);
    assertEquals(i++, spreadOfNumbers[0][2][3]);

    assertEquals(i++, spreadOfNumbers[0][1][3]);
    assertEquals(i++, spreadOfNumbers[0][0][3]);
    assertEquals(i++, spreadOfNumbers[0][0][2]);
    assertEquals(i++, spreadOfNumbers[0][0][1]);

    assertEquals(i++, spreadOfNumbers[0][1][1]);
    assertEquals(i++, spreadOfNumbers[0][2][1]);
    assertEquals(i++, spreadOfNumbers[0][2][2]);
    assertEquals(i++, spreadOfNumbers[0][1][2]);

    // 17
    assertEquals(i++, spreadOfNumbers[1][1][2]);
    assertEquals(i++, spreadOfNumbers[1][2][2]);
    assertEquals(i++, spreadOfNumbers[1][2][1]);
    assertEquals(i++, spreadOfNumbers[1][1][1]);

    assertEquals(i++, spreadOfNumbers[1][0][1]);
    assertEquals(i++, spreadOfNumbers[1][0][2]);
    assertEquals(i++, spreadOfNumbers[1][0][3]);
    assertEquals(i++, spreadOfNumbers[1][1][3]);

    assertEquals(i++, spreadOfNumbers[1][2][3]);
    assertEquals(i++, spreadOfNumbers[1][3][3]);
    assertEquals(i++, spreadOfNumbers[1][3][2]);
    assertEquals(i++, spreadOfNumbers[1][3][1]);

    assertEquals(i++, spreadOfNumbers[1][3][0]);
    assertEquals(i++, spreadOfNumbers[1][2][0]);
    assertEquals(i++, spreadOfNumbers[1][1][0]);
    assertEquals(i++, spreadOfNumbers[1][0][0]);

    // 33
    assertEquals(i++, spreadOfNumbers[2][0][0]);
    assertEquals(i++, spreadOfNumbers[2][1][0]);
    assertEquals(i++, spreadOfNumbers[2][2][0]);
    assertEquals(i++, spreadOfNumbers[2][3][0]);

    assertEquals(i++, spreadOfNumbers[2][3][1]);
    assertEquals(i++, spreadOfNumbers[2][3][2]);
    assertEquals(i++, spreadOfNumbers[2][3][3]);
    assertEquals(i++, spreadOfNumbers[2][2][3]);

    assertEquals(i++, spreadOfNumbers[2][1][3]);
    assertEquals(i++, spreadOfNumbers[2][0][3]);
    assertEquals(i++, spreadOfNumbers[2][0][2]);
    assertEquals(i++, spreadOfNumbers[2][0][1]);

    assertEquals(i++, spreadOfNumbers[2][1][1]);
    assertEquals(i++, spreadOfNumbers[2][2][1]);
    assertEquals(i++, spreadOfNumbers[2][2][2]);
    assertEquals(i++, spreadOfNumbers[2][1][2]);

    // 39
    assertEquals(i++, spreadOfNumbers[3][1][2]);
    assertEquals(i++, spreadOfNumbers[3][2][2]);
    assertEquals(i++, spreadOfNumbers[3][2][1]);
    assertEquals(i++, spreadOfNumbers[3][1][1]);

    assertEquals(i++, spreadOfNumbers[3][0][1]);
    assertEquals(i++, spreadOfNumbers[3][0][2]);
    assertEquals(i++, spreadOfNumbers[3][0][3]);
    assertEquals(i++, spreadOfNumbers[3][1][3]);

    assertEquals(i++, spreadOfNumbers[3][2][3]);
    assertEquals(i++, spreadOfNumbers[3][3][3]);
    assertEquals(i++, spreadOfNumbers[3][3][2]);
    assertEquals(i++, spreadOfNumbers[3][3][1]);

    assertEquals(i++, spreadOfNumbers[3][3][0]);
    assertEquals(i++, spreadOfNumbers[3][2][0]);
    assertEquals(i++, spreadOfNumbers[3][1][0]);
    assertEquals(i++, spreadOfNumbers[3][0][0]);
  }
}
