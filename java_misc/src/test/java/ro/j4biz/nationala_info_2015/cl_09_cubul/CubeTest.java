package ro.j4biz.nationala_info_2015.cl_09_cubul;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CubeTest {

  @Test
  public void test3() {
    Cube cube = new Cube(3);
    assertEquals("2 2 2", cube.getPostion(10));
    assertEquals("3 3 3", cube.getPostion(23));
  }

  @Test
  public void test4() {
    Cube cube = new Cube(4);
    assertEquals("1 3 3", cube.getPostion(15));
    assertEquals("1 2 3", cube.getPostion(16));
    assertEquals("2 2 3", cube.getPostion(17));
    assertEquals("2 3 4", cube.getPostion(25));
    assertEquals("2 1 1", cube.getPostion(32));
    assertEquals("3 1 1", cube.getPostion(33));
    assertEquals("3 3 2", cube.getPostion(46));
    assertEquals("3 2 3", cube.getPostion(48));
    assertEquals("4 2 3", cube.getPostion(49));
  }

  @Test
  public void testPrimes3() {
    Cube cube = new Cube(3);
    assertEquals(4, cube.getPrimeFata(1));
    assertEquals(3, cube.getPrimeFata(2));
    assertEquals(4, cube.getPrimeFata(3));
    assertEquals(3, cube.getPrimeFata(4));
  }

  @Test
  public void testPrimes4() {
    Cube cube = new Cube(4);
    assertEquals(6, cube.getPrimeFata(1));
    assertEquals(6, cube.getPrimeFata(2));
    assertEquals(3, cube.getPrimeFata(3));
    assertEquals(5, cube.getPrimeFata(4));
  }

  @Test
  public void testPrimes200() {
    long t0 = System.currentTimeMillis();
    Cube cube = new Cube(200);
    assertEquals(2747, cube.getPrimeFata(1));
    assertEquals(2736, cube.getPrimeFata(2));
    assertEquals(2712, cube.getPrimeFata(3));
    assertEquals(2689, cube.getPrimeFata(4));
    long t1 = System.currentTimeMillis();
    System.out.println(t1 - t0);
  }
}
