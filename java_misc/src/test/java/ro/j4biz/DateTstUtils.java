package ro.j4biz;

import java.util.Date;

import org.joda.time.DateTime;

public class DateTstUtils {
  private static int year = 2015;
  private static int month = 4;

  public static Date getDate(int day) {
    DateTime dateTime = new DateTime(year, month, day, 0, 0, 0, 0);
    return dateTime.toDate();
  }

}
