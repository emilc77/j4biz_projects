package ro.j4biz.text_html_searcher;

import java.io.IOException;
import java.io.InputStream;

import junit.framework.Assert;

import org.junit.Test;

import com.google.common.io.ByteStreams;

public class TextHtmlSearcherTest {

  @Test
  public void testOverlap1() throws IOException {
    InputStream inStream = TextHtmlSearcherTest.class.getResourceAsStream("/text_html_searcher/text1.html");
    byte[] bytes = ByteStreams.toByteArray(inStream);
    String text = new String(bytes, "UTF-8");

    TextHtmlSearcher searcher = new TextHtmlSearcher(text, "<head", "</head", "<");
    Assert.assertEquals(6, searcher.getStartIndex());
    Assert.assertEquals(82, searcher.getEndIndex());

    searcher = new TextHtmlSearcher(text, "<head", "</cacamaca", "<");
    Assert.assertEquals(-1, searcher.getStartIndex());
    Assert.assertEquals(-1, searcher.getEndIndex());

    searcher = new TextHtmlSearcher(text, "<td", "</td", "<");
    Assert.assertEquals(-1, searcher.getStartIndex());
    Assert.assertEquals(-1, searcher.getEndIndex());

    searcher = new TextHtmlSearcher(text, "<head", "</head", "cacamaca");
    Assert.assertEquals(-1, searcher.getStartIndex());
    Assert.assertEquals(-1, searcher.getEndIndex());

    searcher = new TextHtmlSearcher(text, "<head", "</head", "src=\"jquery-2.1.4.js\"");
    Assert.assertEquals(6, searcher.getStartIndex());
    Assert.assertEquals(82, searcher.getEndIndex());
  }

}
