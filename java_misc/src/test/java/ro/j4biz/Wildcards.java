package ro.j4biz;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

//http://stackoverflow.com/questions/19107165/search-with-wildcard-in-a-collection-of-string
public class Wildcards {
  private final String pattern;

  public Wildcards(String pattern) {
    // this.pattern = pattern.replaceAll("\\*", "\\\\w*");

    // appendam la inceput si sfarsit oricate caractere.
    this.pattern = ".*" + pattern.replace('?', '.').replaceAll("\\*", ".*") + ".*";
  }

  public boolean matchesOneLine(String text) {
    if (text == null) {
      return false;
    }

    return text.matches(pattern);
  }

  public boolean matchesMultipleLine(String text) {
    if (text == null) {
      return false;
    }

    for (String line : toLines(text)) {
      if (line.matches(pattern)) {
        return true;
      }
    }

    return false;
  }

  private List<String> toLines(String text) {
    List<String> returnList = new ArrayList<String>();
    if (text == null) {
      return returnList;
    }

    BufferedReader br = new BufferedReader(new StringReader(text));
    try {
      while (true) {
        String line = br.readLine();
        if (line == null) {
          break;
        }
        returnList.add(line);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      try {
        br.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    return returnList;
  }

}
