package ro.j4biz;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.google.common.io.BaseEncoding;
import com.google.common.io.Files;

public class Md5Main {
  public static void main(String[] args) throws NoSuchAlgorithmException, IOException {
    File folder = new File("C:\\My Data\\Temp\\");
    digestFolder(folder);
  }

  private static void digestFolder(File folder) throws IOException, NoSuchAlgorithmException {
    File[] listFiles = folder.listFiles();
    for (int i = 0; i < listFiles.length; i++) {
      File file = listFiles[i];
      if (!file.isFile()) {
        continue;
      }

      digestFile(file);
    }
  }

  private static void digestFile(File file) throws IOException, NoSuchAlgorithmException {
    byte[] bytes = Files.toByteArray(file);
    MessageDigest instance = MessageDigest.getInstance("MD5");
    String digest = BaseEncoding.base64().encode(instance.digest(bytes));

    System.out.println(digest + "\t\t" + file);
  }

}
