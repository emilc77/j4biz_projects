package ro.j4biz.plaid;

/**
 * Created by emil on 8/9/14. See: https://api.clickbank.com/rest/1.3/orders
 */
public class OrdersDownloader {
  // private static final Log LOG = LogFactory.getLog(OrdersDownloader.class);
  //
  // private static final String URL_ORDERS_LIST = "https://api.clickbank.com/rest/1.3/orders/list";
  // private static final String URL_ORDERS_COUNT = "https://api.clickbank.com/rest/1.3/orders/count";
  //
  // private OrdersDownloader() {}
  //
  // public static List<byte[]> retrieveAllOrders(String developerApiKey, String clerkApiKey, Date startDate, Date
  // endDate) {
  // if (startDate == null) {
  // // TODO: limit to 1 sept 2014
  // startDate = SimpleDateFormatUtil.parseYYYY_MM_DD("2014-09-01");
  // }
  //
  // if (startDate != null) {
  // // From the API:
  // // If a startDate is specified, you must also specify an endDate.
  // if (endDate == null) {
  // // tomorrow
  // endDate = DateTimeUtils.addDays(new Date(), 1);
  // }
  // }
  //
  //
  // if (endDate != null) {
  // // From the API:
  // // If an endDate is specified, you must also specify a startDate.
  // if (startDate == null) {
  // // yesterday
  // startDate = DateTimeUtils.addDays(new Date(), -1);
  // }
  // }
  //
  // Map<String, String> mapParams = new HashMap<>();
  // if (startDate != null) {
  // mapParams.put("startDate", SimpleDateFormatUtil.formatYYYY_MM_DD(startDate));
  // }
  // if (endDate != null) {
  // mapParams.put("endDate", SimpleDateFormatUtil.formatYYYY_MM_DD(endDate));
  // }
  //
  // List<byte[]> list = new ArrayList<>();
  //
  // for (int page = 1; ; page++) {
  // Tuple2<Integer, byte[]> tuple = doGet(URL_ORDERS_LIST, mapParams, developerApiKey, clerkApiKey, page);
  // list.add(tuple.elem2);
  //
  // if (200 == tuple.elem1) {
  // break;
  // }
  //
  // try {
  // // No more than 5 calls / second.
  // // That's why we slow it down.
  // Thread.sleep(300);
  // } catch (InterruptedException e) {
  // Throwables.propagate(e);
  // }
  // }
  //
  // return list;
  // }
  //
  // public static int retrieveOrdersCount(String developerApiKey, String clerkApiKey, Date startDate) {
  // if (startDate == null) {
  // // TODO: limit to 1 sept 2014
  // startDate = SimpleDateFormatUtil.parseYYYY_MM_DD("2014-09-01");
  // }
  //
  // Map<String, String> mapParams = new HashMap<>();
  // if (startDate != null) {
  // mapParams.put("startDate", SimpleDateFormatUtil.formatYYYY_MM_DD(startDate));
  // }
  //
  // Tuple2<Integer, byte[]> tuple = doGet(URL_ORDERS_COUNT, mapParams, developerApiKey, clerkApiKey, -1);
  // checkArgument(200 == tuple.elem1);
  //
  // try {
  // String strCount = new String(tuple.elem2, "UTF-8");
  // return Integer.parseInt(strCount);
  // } catch (UnsupportedEncodingException e) {
  // throw Throwables.propagate(e);
  // }
  // }
  //
  // public static Tuple2<Integer, byte[]> doGet(String url, Map<String, String> mapParams, String developerApiKey,
  // String clerkApiKey, int page) {
  // CloseableHttpClient httpclient = HttpClients.createDefault();
  //
  // // TODO:
  // // httpclient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 20000);
  // // httpclient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 10000);
  // // httpclient.getParams().setParameter(CoreConnectionPNames.SO_LINGER, 20);
  //
  // try {
  // List<NameValuePair> params = new ArrayList<NameValuePair>();
  //
  // if (mapParams != null) {
  // for (Map.Entry<String, String> entry : mapParams.entrySet()) {
  // params.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
  // }
  // }
  //
  // // ------------ params ------------
  // String queryParams = URLEncodedUtils.format(params, HTTP.UTF_8);
  // LOG.info("queryParams=" + queryParams);
  //
  // HttpGet httpGet;
  // if (StringUtils.isBlank(queryParams)) {
  // httpGet = new HttpGet(url);
  // } else {
  // httpGet = new HttpGet(url + "?" + queryParams);
  // }
  // LOG.info("GET:\t" + httpGet.getURI());
  //
  // // << DEVELOPER KEY >>:<< API KEY >>
  // httpGet.addHeader("Authorization", developerApiKey + ":" + clerkApiKey);
  // httpGet.addHeader("Accept", "application/xml");
  // if (page > 1) {
  // httpGet.addHeader("Page", Integer.toString(page));
  // }
  //
  // LOG.debug("Executing request " + httpGet.getRequestLine());
  // CloseableHttpResponse response = httpclient.execute(httpGet);
  // return processResponse(url, response);
  // } catch (IOException e) {
  // throw Throwables.propagate(e);
  //
  // } finally {
  // try {
  // httpclient.close();
  // } catch (IOException e) {
  // LOG.error("when closing httpclient", e);
  // }
  // }
  // }
  //
  // private static Tuple2<Integer, byte[]> processResponse(String url, CloseableHttpResponse response) {
  // try {
  // LOG.debug(response.getStatusLine());
  //
  // // -------------- HANDLE RESPONSE ---------------------
  // int sc = response.getStatusLine().getStatusCode();
  // if (sc != 200 && sc != 206) {
  // String err = "Error at '" + url + "' sc != 200/206; sc=" + sc;
  // throw new RuntimeException(err);
  // }
  //
  // // Get hold of the response entity
  // HttpEntity entity = response.getEntity();
  // if (entity == null) {
  // throw new RuntimeException("entity == null");
  // }
  //
  // byte[] bytes;
  // try (InputStream content = entity.getContent()) {
  // if (content == null) {
  // throw new RuntimeException("content == null");
  // }
  // bytes = IOUtils.getByteArrayFromInStream(content).toByteArray();
  // }
  //
  // return new Tuple2<Integer, byte[]>(sc, bytes);
  //
  // } catch (IOException e) {
  // throw Throwables.propagate(e);
  //
  // } finally {
  // try {
  // response.close();
  // } catch (IOException e) {
  // LOG.error("when closing response", e);
  // }
  // }
  // }

}
