package ro.j4biz.cache_date_vat_percent;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.Date;

public class VatInterval implements Serializable {
  private static final long serialVersionUID = 1866690559692586522L;

  private final Date start;
  private final Date end;
  private final double percent;

  public VatInterval(Date start, Date end, double percent) {
    checkNotNull(start);
    checkArgument(percent > 0.0001);

    if (end != null) {
      checkArgument(start.getTime() < end.getTime());
    }

    this.start = start;
    this.end = end;
    this.percent = percent;
  }

  public boolean matchesInterval(Date date) {
    checkNotNull(date);

    if (end == null) {
      if (start.getTime() <= date.getTime()) {
        return true;
      } else {
        return false;
      }
    } else {
      // intervalul es [xxx, yyy)
      if (start.getTime() <= date.getTime() && date.getTime() < end.getTime()) {
        return true;
      } else {
        return false;
      }
    }
  }

  public double getPercent() {
    return percent;
  }

}
