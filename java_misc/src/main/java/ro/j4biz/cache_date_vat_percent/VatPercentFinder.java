package ro.j4biz.cache_date_vat_percent;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VatPercentFinder implements Serializable {
  private static final long serialVersionUID = -1847894565676419631L;

  private List<VatInterval> intervals = new ArrayList<>();

  public void addInterval(Date start, Date end, double percent) {
    intervals.add(new VatInterval(start, end, percent));
  }

  public double getVatPercent(Date date) {
    double percent = -100;
    for (VatInterval interval : intervals) {
      if (interval.matchesInterval(date)) {
        if (percent >= -0.0001) {
          throw new RuntimeException("At lease 2 intervals contain this date: " + date);
        }

        percent = interval.getPercent();
      }
    }
    if (percent >= -0.0001) {
      return percent;
    }
    throw new RuntimeException("No interval contains this date: " + date);
  }
}
