package ro.j4biz;

import java.text.DecimalFormat;

public class PrinderePlasaPiscina {
    public static void main(String[] args) {
        double raza = 247;
        int numarulPrizelor = 16;
        DecimalFormat df = new DecimalFormat("####0");

        for (int i = 0; i < numarulPrizelor; i++) {
            double unghiul = 2 * Math.PI * (i / 16.0);

            double x = Math.sin(unghiul) * raza;
            double y = Math.cos(unghiul) * raza;

            System.out.println(
                    // unghiul + ", " +
                    df.format(x) + "("
                            + Math.round(x / 5)
                            + ")\t" + df.format(y)
                            + "("
                            + Math.round(y / 5)
                            + ")");
        }
    }
}
