package ro.j4biz.text_html_searcher;

import com.google.common.base.Preconditions;

public class TextHtmlSearcher {
  private final String text;
  private final String startToken;
  private final String endToken;
  private final String innerText;

  private boolean processed = false;
  private int startIndex = -1;
  private int endIndex = -1;

  public TextHtmlSearcher(String text, String startToken, String endToken, String innerText) {
    // TODO:
    Preconditions.checkNotNull(text);
    Preconditions.checkNotNull(startToken);
    Preconditions.checkNotNull(endToken);

    this.text = text;
    this.startToken = startToken;
    this.endToken = endToken;
    this.innerText = innerText;
  }

  public int getStartIndex() {
    process();

    return startIndex;
  }

  public int getEndIndex() {
    process();

    return endIndex;
  }

  private void process() {
    if (processed) {
      return;
    }
    processed = true;

    search(0);
  }

  private void search(int startPosition) {
    startIndex = text.indexOf(startToken, startPosition);
    if (startIndex == -1) {
      notFound();
      return;
    }

    endIndex = text.indexOf(endToken, startIndex);
    if (endIndex == -1) {
      notFound();
      return;
    }

    String substring = text.substring(startIndex, endIndex);
    if (substring.contains(innerText)) {
      // L-am afalat.
      return;
    }

    // cautam de la noul index.
    search(endIndex);
  }

  private void notFound() {
    startIndex = -1;
    endIndex = -1;
  }

}
