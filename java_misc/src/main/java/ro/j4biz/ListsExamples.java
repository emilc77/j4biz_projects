package ro.j4biz;

import java.util.List;

import com.google.common.collect.Lists;

public class ListsExamples {
  public static void main(String[] args) {
    List<String> list = Lists.<String> newArrayList("abc", "1212");
    System.out.println(list);
  }
}
