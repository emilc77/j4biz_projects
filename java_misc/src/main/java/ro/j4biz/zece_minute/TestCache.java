package ro.j4biz.zece_minute;

import java.util.Date;
import java.util.Random;

public class TestCache {
  private static long lastRefresh = 0;

  // expire every one minute
  private static final long EXPIRATION = 1L * 60 * 1000;

  private static String cachedValue;

  public static String getInstance() {
    if (isExpired()) {
      cachedValue = null;
    }

    if (cachedValue == null) {
      cachedValue = new Date().toString();
      lastRefresh = System.currentTimeMillis();
    }

    return cachedValue;
  }

  private static boolean isExpired() {
    if (lastRefresh == 0) {
      return true;
    }

    long t1 = System.currentTimeMillis();
    long delta = t1 - lastRefresh;
    if (delta > EXPIRATION) {
      return true;
    } else {
      return false;
    }
  }

  public static void main(String[] args) throws InterruptedException {
    Random random = new Random();
    for (int i = 0; i < 3000; i++) {
      int sleep = random.nextInt(200);
      Thread.sleep(sleep);
      System.out.println(TestCache.getInstance());
    }
  }
}
