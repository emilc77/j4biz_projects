package ro.j4biz.csv_parser;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class MyCsvParser {
  private static final String HEADER_PAGE = "PAGE";
  private byte[] file;
  private Map<String, CsvData> data = new HashMap<String, CsvData>();
  private boolean parsed;

  private String[] header;

  public MyCsvParser(byte[] file) {
    this.file = file;
  }

  public Map<String, CsvData> getData() {
    return data;
  }

  private void parser() throws IOException {
    if (parsed) {
      return;
    }
    parsed = true;

    // Reader reader = new InputStreamReader(new BOMInputStream(url.openStream()), "UTF-8");
    Reader reader = new InputStreamReader(new ByteArrayInputStream(file), "UTF-8");
    CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withHeader());

    try {
      for (CSVRecord record : parser) {
        parseLine(record);
      }

    } finally {
      parser.close();
      reader.close();
    }
  }

  private void parseLine(CSVRecord record) {
    Iterator<String> it = record.iterator();
  }
}
