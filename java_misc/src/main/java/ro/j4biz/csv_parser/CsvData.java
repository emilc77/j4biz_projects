package ro.j4biz.csv_parser;

import java.io.Serializable;

public class CsvData implements Serializable {
  private String[] header;
  private String[][] data;

  public String[] getHeader() {
    return header;
  }

  public void setHeader(String[] header) {
    this.header = header;
  }

  public String[][] getData() {
    return data;
  }

  public void setData(String[][] data) {
    this.data = data;
  }
}
