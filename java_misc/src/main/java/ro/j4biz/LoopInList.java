package ro.j4biz;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ra52tk on 09.08.2016.
 */
public class LoopInList {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("A");
        list.add("B");
        list.add("C");

        Iterator<String> it1 = list.iterator();
        Iterator<String> it2 = list.iterator();

        System.out.println(it1.next());
        System.out.println(it2.next());
        System.out.println(it1.next());
        System.out.println(it1.next());

        System.out.println(it2.next());
        System.out.println(it2.next());
    }
}
