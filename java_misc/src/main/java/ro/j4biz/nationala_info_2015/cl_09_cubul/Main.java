package ro.j4biz.nationala_info_2015.cl_09_cubul;

public class Main {

  public static void main(String[] args) {
    int requirementType = Integer.parseInt(args[0]);

    String[] line2 = args[1].split(" ");
    int laturaCub = Integer.parseInt(line2[0]);
    int searchedNumber = Integer.parseInt(line2[1]);

    if (requirementType == 1) {
      Cube cube = new Cube(laturaCub);
      System.out.println(cube.getPostion(searchedNumber));

    } else if (requirementType == 2) {
      Cube cube = new Cube(laturaCub);
      System.out.println(cube.getPrimeFata(1));
      System.out.println(cube.getPrimeFata(2));
      System.out.println(cube.getPrimeFata(3));
      System.out.println(cube.getPrimeFata(4));

      // TODO:
    } else {
      throw new RuntimeException();
    }
  }

}
