package ro.j4biz.nationala_info_2015.cl_09_cubul;

public class CubeSpreadCalculator {
  private final int laturaCub;
  private final int[][][] spreadOfNumbers;

  private final boolean[][][] notPrimes;
  private final boolean[] notPrimesAll;

  private int planIndex;
  private int currentValue;
  private int positionX = 0;
  private int positionY = 0;

  // right = 1, bottom = 2, left = 3, top = 4
  int currentDirection = 1;

  public CubeSpreadCalculator(int laturaCub) {
    this.laturaCub = laturaCub;
    this.spreadOfNumbers = new int[laturaCub][laturaCub][laturaCub];
    this.notPrimes = new boolean[laturaCub][laturaCub][laturaCub];
    this.notPrimesAll = new boolean[laturaCub * laturaCub * laturaCub];
  }

  public void computeSpread() {
    for (planIndex = 0; planIndex < laturaCub; planIndex++) {
      if (planIndex % 2 == 0) {
        computePlan();
      } else {
        mirrorPreviousPlan();
      }
    }
  }

  private void mirrorPreviousPlan() {
    int[][] previousPlanData = spreadOfNumbers[planIndex - 1];

    // start index
    int startIndex = planIndex * laturaCub * laturaCub;

    for (int i = 0; i < laturaCub; i++) {
      for (int j = 0; j < laturaCub; j++) {
        int currentIndex = startIndex + (startIndex - previousPlanData[i][j]) + 1;
        setValue(planIndex, i, j, currentIndex);
      }
    }
  }

  private void computePlan() {
    positionX = 0;
    positionY = 0;
    currentDirection = 1;

    // start index
    int startIndex = planIndex * laturaCub * laturaCub;
    // data on this plan
    int[][] planData = spreadOfNumbers[planIndex];

    // set starting point
    setValue(planIndex, positionX, positionY, startIndex + 1);

    int maxItemsOnPlan = laturaCub * laturaCub;
    for (int i = 1; i < maxItemsOnPlan; i++) {
      currentValue = i + startIndex + 1;

      if (currentDirection == 1) {
        if (canGoRight(planData)) {
          goRight();
        } else if (canGoBottom(planData)) {
          goBottom();
        } else if (canGoLeft(planData)) {
          goLeft();
        } else if (canGoTop(planData)) {
          goTop();
        }

      } else if (currentDirection == 2) {
        if (canGoBottom(planData)) {
          goBottom();
        } else if (canGoLeft(planData)) {
          goLeft();
        } else if (canGoTop(planData)) {
          goTop();
        } else if (canGoRight(planData)) {
          goRight();
        }

      } else if (currentDirection == 3) {
        if (canGoLeft(planData)) {
          goLeft();
        } else if (canGoTop(planData)) {
          goTop();
        } else if (canGoRight(planData)) {
          goRight();
        } else if (canGoBottom(planData)) {
          goBottom();
        }

      } else if (currentDirection == 4) {
        if (canGoTop(planData)) {
          goTop();
        } else if (canGoRight(planData)) {
          goRight();
        } else if (canGoBottom(planData)) {
          goBottom();
        } else if (canGoLeft(planData)) {
          goLeft();
        }

      }

    }
  }

  private boolean canGoRight(int[][] planData) {
    if (positionX + 1 >= laturaCub) {
      return false;
    }
    if (planData[positionX + 1][positionY] != 0) {
      return false;
    }

    return currentDirection == 1 || currentDirection == 4;
  }

  private void goRight() {
    positionX = positionX + 1;
    currentDirection = 1;

    setValue(planIndex, positionX, positionY, currentValue);
  }

  private boolean canGoBottom(int[][] planData) {
    if (positionY + 1 >= laturaCub) {
      return false;
    }
    if (planData[positionX][positionY + 1] != 0) {
      return false;
    }

    return currentDirection == 2 || currentDirection == 1;
  }

  private void goBottom() {
    positionY = positionY + 1;
    currentDirection = 2;

    setValue(planIndex, positionX, positionY, currentValue);
  }

  private boolean canGoLeft(int[][] planData) {
    if (positionX - 1 < 0) {
      return false;
    }
    if (planData[positionX - 1][positionY] != 0) {
      return false;
    }

    return currentDirection == 3 || currentDirection == 2;
  }

  private void goLeft() {
    positionX = positionX - 1;
    currentDirection = 3;

    setValue(planIndex, positionX, positionY, currentValue);
  }

  private boolean canGoTop(int[][] planData) {
    if (positionY - 1 < 0) {
      return false;
    }
    if (planData[positionX][positionY - 1] != 0) {
      return false;
    }

    return currentDirection == 4 || currentDirection == 3;
  }

  private void goTop() {
    positionY = positionY - 1;
    currentDirection = 4;

    setValue(planIndex, positionX, positionY, currentValue);
  }

  public int[][][] getSpreadOfNumbers() {
    return spreadOfNumbers;
  }

  public boolean[][][] getSpreadOfNonPrimeNumbers() {
    return notPrimes;
  }

  private void setValue(int h, int x, int y, int currentIndex) {
    spreadOfNumbers[h][x][y] = currentIndex;

    if (currentIndex == 1) {
      // 1 nu este prim
      notPrimes[h][x][y] = true;

    } else {
      notPrimes[h][x][y] = notPrimesAll[currentIndex - 1];

      for (int i = 2;; i++) {
        int index = currentIndex * i;
        if (index >= notPrimesAll.length) {
          break;
        }
        notPrimesAll[index - 1] = true;
      }
    }

  }

}
