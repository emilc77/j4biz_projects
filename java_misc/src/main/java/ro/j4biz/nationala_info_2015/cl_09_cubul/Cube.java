package ro.j4biz.nationala_info_2015.cl_09_cubul;

public class Cube {
  private final int laturaCub;
  private final int[][][] spreadOfNumbers;
  private final boolean[][][] spreadOfNonPrimeNumbers;

  public Cube(int laturaCub) {
    this.laturaCub = laturaCub;

    CubeSpreadCalculator calculator = new CubeSpreadCalculator(laturaCub);
    calculator.computeSpread();
    this.spreadOfNumbers = calculator.getSpreadOfNumbers();
    this.spreadOfNonPrimeNumbers = calculator.getSpreadOfNonPrimeNumbers();
  }

  public String getPostion(int number) {
    int planIndex = (number - 1) / (laturaCub * laturaCub);
    int[][] planData = spreadOfNumbers[planIndex];

    for (int i = 0; i < laturaCub; i++) {
      for (int j = 0; j < laturaCub; j++) {
        if (planData[i][j] == number) {
          return (planIndex + 1) + " " + (i + 1) + " " + (j + 1);
        }
      }
    }

    return null;
  }

  public int getPrimeFata(int indexFata) {
    int primes = 0;
    if (indexFata == 1) {
      // spatele
      for (int h = 0; h < laturaCub; h++) {
        for (int i = 0; i < laturaCub; i++) {
          // for (int j = 0; j < laturaCub; j++) {
          if (!spreadOfNonPrimeNumbers[h][i][0]) {
            // System.out.println(spreadOfNumbers[h][i][0]);
            primes++;
          }
          // }
        }
      }

    } else if (indexFata == 2) {
      // dreapta
      for (int h = 0; h < laturaCub; h++) {
        // for (int i = 0; i < laturaCub; i++) {
        for (int j = 0; j < laturaCub; j++) {
          if (!spreadOfNonPrimeNumbers[h][laturaCub - 1][j]) {
            // System.out.println(spreadOfNumbers[h][laturaCub - 1][j]);
            primes++;
          }
        }
        // }
      }

    } else if (indexFata == 3) {
      // spatele
      for (int h = 0; h < laturaCub; h++) {
        for (int i = 0; i < laturaCub; i++) {
          // for (int j = 0; j < laturaCub; j++) {
          if (!spreadOfNonPrimeNumbers[h][i][laturaCub - 1]) {
            // System.out.println(spreadOfNumbers[h][i][laturaCub - 1]);
            primes++;
          }
          // }
        }
      }

    } else if (indexFata == 4) {
      // spatele
      for (int h = 0; h < laturaCub; h++) {
        // for (int i = 0; i < laturaCub; i++) {
        for (int j = 0; j < laturaCub; j++) {
          if (!spreadOfNonPrimeNumbers[h][0][j]) {
            // System.out.println(spreadOfNumbers[h][0][j]);
            primes++;
          }
        }
        // }
      }

    } else {
      throw new RuntimeException();
    }

    return primes;

  }

}
