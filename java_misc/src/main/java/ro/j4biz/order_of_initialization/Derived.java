package ro.j4biz.order_of_initialization;

public class Derived extends Base {
    Marker field = new Marker("Derived field");
    static Marker staticField = new Marker("Derived staticField");
    {
        new Marker("Derived initializationBlock");
    }
    static {
        new Marker("Derived staticInitializationBlock");
    }

    Derived() {
        new Marker("Derived constructor");
    }
}
