package ro.j4biz.order_of_initialization;

public class Base {
    Marker field = new Marker("Base field");
    static Marker staticField = new Marker("Base staticField");
    {
        new Marker("Base initializationBlock");
    }
    static {
        new Marker("Base staticInitializationBlock");
    }

    Base() {
        new Marker("Base constructor");
    }
}
