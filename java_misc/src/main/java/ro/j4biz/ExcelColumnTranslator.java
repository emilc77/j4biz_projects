package ro.j4biz;

/**
 * Created by ra52tk on 18.07.2016.
 */
public class ExcelColumnTranslator {
    public static final char[] LETTERS_USED = ("ABCDE" +
            "FGHIJ" +
            "KLMNO" +
            "PQRST" +
            "UVWXY" +
            "Z").toCharArray();

    public static String translateNumericToString(int columnIndex) {
        if (columnIndex < 0) {
            throw new IllegalArgumentException("columnIndex < 0; columnIndex=" + columnIndex);
        }

        int radix = LETTERS_USED.length;
        // noi lucram in gama cu 0.. si vreau sa trecem in naturale
        int fullNumber = columnIndex;

        StringBuilder b = new StringBuilder();
        while (true) {
            int catul = fullNumber / radix;
            int restul = fullNumber % radix;

            b.append(LETTERS_USED[restul]);
            if (catul == 0) {
                break;
            }

            fullNumber = catul - 1;
        }

        return b.reverse().toString();
    }
}
