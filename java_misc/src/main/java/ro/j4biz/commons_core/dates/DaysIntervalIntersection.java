package ro.j4biz.commons_core.dates;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Interval;

public class DaysIntervalIntersection {

  private final DateTime start1;
  private final DateTime end1;
  private final DateTime start2;
  private final DateTime end2;

  public DaysIntervalIntersection(Date start1, Date end1, Date start2, Date end2) {
    checkNotNull(start1);
    checkNotNull(end1);
    checkNotNull(start2);
    checkNotNull(end2);
    checkArgument(start1.getTime() <= end1.getTime(), "start1 > end1");
    checkArgument(start2.getTime() <= end2.getTime(), "start2 > end2");

    this.start1 = nullHourMinutesSecondsMillis(start1);
    this.end1 = nullHourMinutesSecondsMillis(end1);
    this.start2 = nullHourMinutesSecondsMillis(start2);
    this.end2 = nullHourMinutesSecondsMillis(end2);
  }

  public DaysIntervalIntersection(DateTime start1, DateTime end1, DateTime start2, DateTime end2) {
    checkNotNull(start1);
    checkNotNull(end1);
    checkNotNull(start2);
    checkNotNull(end2);
    checkArgument(start1.getMillis() <= end1.getMillis(), "start1 > end1");
    checkArgument(start2.getMillis() <= end2.getMillis(), "start2 > end2");

    this.start1 = nullHourMinutesSecondsMillis(start1);
    this.end1 = nullHourMinutesSecondsMillis(end1);
    this.start2 = nullHourMinutesSecondsMillis(start2);
    this.end2 = nullHourMinutesSecondsMillis(end2);
  }

  private static DateTime nullHourMinutesSecondsMillis(Date date) {
    return nullHourMinutesSecondsMillis(new DateTime(date));
  }

  private static DateTime nullHourMinutesSecondsMillis(DateTime dateTime) {
    return dateTime.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0);
  }

  public List<Date> getIntersectionAsDates() {
    List<Date> returnList = new ArrayList<>();

    for (DateTime dateTime : getIntersectionAsDateTimes()) {
      returnList.add(dateTime.toDate());
    }

    return returnList;
  }

  public List<DateTime> getIntersectionAsDateTimes() {
    List<DateTime> returnList = new ArrayList<>();

    Interval interval1 = new Interval(start1.getMillis(), end1.getMillis());
    Interval interval2 = new Interval(start2.getMillis(), end2.getMillis());

    if (interval1.abuts(interval2)) {
      // avem un element in comun
      if (interval1.getStartMillis() == interval2.getEndMillis()) {
        returnList.add(interval1.getStart());
      } else if (interval2.getStartMillis() == interval1.getEndMillis()) {
        returnList.add(interval2.getStart());
      } else {
        throw new IllegalStateException("Incorrect gapping");
      }

    } else {
      if (interval1.gap(interval2) != null) {
        // nu avem elemente comune, avem gap

      } else {
        // cazul cel mai "cu carne". Overlap
        Interval overlap = interval1.overlap(interval2);
        checkNotNull(overlap);

        DateTime day = overlap.getStart();
        do {
          returnList.add(day);
          day = day.plusDays(1);
        } while (day.getMillis() <= overlap.getEndMillis());
      }

    }

    return returnList;
  }

}
