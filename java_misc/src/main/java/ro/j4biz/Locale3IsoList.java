package ro.j4biz;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.MissingResourceException;
import java.util.TreeMap;

public class Locale3IsoList {
  public static void main(String[] args) throws IOException {
    System.out.println(Locale.US.getISO3Country());

    String[] countryCodes = Locale.getISOCountries();
    for (String isoCode : countryCodes) {
      Locale locale = new Locale(isoCode);

      System.out.println(isoCode.toUpperCase() + "(\"" + locale.getDisplayLanguage(locale) + "\"),");
      System.out.println(locale.getCountry());
      System.out.println(locale.getDisplayCountry());
      System.out.println(locale.getDisplayCountry(Locale.US));
      System.out.println(locale.getISO3Country());

      System.out.println("   ----------------------   ");
    }

    System.out.println("   ==================   ");

    Map<String, String> map = new TreeMap<>();
    for (Locale locale : Locale.getAvailableLocales()) {
      try {
        map.put(locale.getISO3Country(), locale.getDisplayCountry(Locale.US));
      } catch (MissingResourceException e) {
        continue;
      }
    }

    for (Entry<String, String> entry : map.entrySet()) {
      System.out.println(entry.getKey() + ":" + entry.getValue());
    }

    System.out.println("   ==================#^&#%^&#$%&#$%^#$% ===============   ");

    InputStream inStream = Locale3IsoList.class.getResourceAsStream("/iso3_countries.txt");
    BufferedReader br = new BufferedReader(new InputStreamReader(inStream));
    while (true) {
      String line = br.readLine();
      if (line == null) {
        break;
      }

      if (line.trim().isEmpty()) {
        continue;
      }

      String code = line.substring(0, 3);
      String name = line.substring(4).trim();
      System.out.println(code + ":" + name);
    }

  }
}
