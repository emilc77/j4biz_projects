package ro.j4biz.problem_steps_recorder;

import java.io.File;

public class Main {
  public static void main(String[] args) {
    File folder = new File(
        "\\\\VPROFAP10041\\UDV00001\\BNK\\ra52tk\\Desktop\\apply_rad_license\\Recorded Problem Steps_files");
    // File folder = new File("\\\\VPROFAP10041\\UDV00001\\BNK\\ra52tk\\Desktop\\apply_rad_license\\test_files");

    if (!folder.exists()) {
      System.err.println("Folder not exist");
      return;
    }
    if (!folder.isDirectory()) {
      System.err.println("Folder not folder");
      return;
    }

    for (File file : folder.listFiles()) {
      // delete size 0 files.
      if (file.length() == 0) {
        boolean deleted = file.delete();
        if (!deleted) {
          System.err.println("Cannot delete file: " + file);
          return;
        }
        continue;
      }

      String fileName = file.getName();
      // rename .tmp to .jpg
      if (fileName.endsWith(".tmp")) {
        fileName = fileName.substring(0, fileName.length() - 4) + ".jpg";
      }

      // replace mhtD318(1).jpg -> decimal
      int endIndex = Math.min(fileName.indexOf("("), fileName.indexOf("."));
      if (!fileName.startsWith("mht")) {
        System.err.println("File not starting with 'mht'");
        return;
      }

      String decimalPart = fileName.substring(3, endIndex);
      int val = Integer.valueOf(decimalPart, 16);
      fileName = fileName.substring(0, 3) + val + fileName.substring(endIndex);

      File newName = new File(folder, fileName);
      boolean renamed = file.renameTo(newName);
      if (!renamed) {
        System.err.println("Cannot rename file: " + file + "; to file:" + newName);
        return;
      }
    }

  }
}
