package ro.j4biz.juridice;

import org.apache.log4j.BasicConfigurator;
import ro.j4biz.commons.core.StringUtils;
import ro.j4biz.commons.core.io.FileUtils;

import java.io.File;
import java.util.List;

public class Main {
    public static void main(final String[] args) throws Exception {
        BasicConfigurator.configure();

        // 2016_02_29-Restantiei-2014-scrisori-premergatoare-somatiilor-de-plata-1
        // 2016_06_23-Restantiei-2014-scrisori-premergatoare-somatiilor-de-plata-2

        // "2016_02_29-Restantiei-2014-scrisori-premergatoare-somatiilor-de-plata-1"
        // "2016_06_23-Restantiei-2014-scrisori-premergatoare-somatiilor-de-plata-2-Bucuresti"
//        String excelName = "2016_07_31-Fisier.lucru.restantieri.2015";
        // "2016_07_31-Fisier.lucru.restantieri.2015-Bucuresti"
// 		String excelName = "2017_06_13-Lista.2016-arad.timis.caras.dolj";
//		String excelName = "2017_06_15-Lista.2016-bihor.brasov.maramures.mures.salaj.satumare";
        // String excelName = "2017_06_15-Lista.2016-ialomita";
        // String excelName = "2018_02_23-restantieri.2016.sortati.pe.judete_BUN_2";
        String excelName = "2018_05_16.3-restantieri.2017.v2-GOOD";

        byte[] bytes = StringUtils.getResourceAsBytes("/" + excelName + ".xls");
        ExcelParser parser = new ExcelParser(bytes);
        List<CompanyBean> lines = parser.getLines();

        File folder = new File("/tmp/" + excelName + "_" + System.currentTimeMillis());
        folder.mkdir();

//		SimpleDateFormat sdfForFiles = new SimpleDateFormat("yyyy_MM_dd");

        for (CompanyBean company : lines) {

            {
                // --- somatia ---
                GeneratorPdfDocAdresa pdfGen = new GeneratorPdfDocAdresa(company);
                byte[] pdf = pdfGen.renderPdf();
                File file = getPdfFile(folder, company.getSocietateNume() + "_somatie");
                FileUtils.writeBytesToFile(pdf, file);
            }

            {
                // --- posta ---
                GeneratorPdfDocPosta pdfGen = new GeneratorPdfDocPosta(company);
                byte[] pdf = pdfGen.renderPdf();
                File file = getPdfFile(folder, company.getSocietateNume() + "_borderou");
                FileUtils.writeBytesToFile(pdf, file);
            }

//			if (StringUtils.isNotEmpty(company.getJudecatoria()) && (company.getFacturaScadenta() != null)) {
//				// --- somatie_judecatorie ---
//				GeneratorPdfSomatieJudecatorie pdfGen = new GeneratorPdfSomatieJudecatorie(company);
//				byte[] pdf = pdfGen.renderPdf();
//                File file = getPdfFile(folder, company.getSocietateNume() + "_somatie_judecatorie");
//				FileUtils.writeBytesToFile(pdf, file);
//			}

        }

    }

    private static File getPdfFile(File folder, String fileNameWithoutExtension) {
        String fileName = FileUtils.normalizeFileName(fileNameWithoutExtension);

        File file = new File(folder, fileName + ".pdf");
        if (!file.exists()) {
            return file;
        }

        // fallback with suffix, we may have the same company multiple times
        for (int i = 0; i < 10; i++) {
            file = new File(folder, fileName + "_" + i + ".pdf");
            if (!file.exists()) {
                return file;
            }
        }

        throw new RuntimeException("cannot create a file with the name: '"
                + fileNameWithoutExtension +  "'");
    }
}
