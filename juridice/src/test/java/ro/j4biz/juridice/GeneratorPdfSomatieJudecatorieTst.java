package ro.j4biz.juridice;

import java.util.Date;

import junit.framework.TestCase;
import ro.j4biz.commons.core.io.FileUtils;

public class GeneratorPdfSomatieJudecatorieTst extends TestCase {

	public void testPdfRenderer() throws Exception {
		CompanyBean companyBean = new CompanyBean();

		companyBean.setSocietateNume("societateNume");
		companyBean.setSocietateStradaNr("societateStradaNr");
		companyBean.setSocietateCodPostal("societateCodPostal");
		companyBean.setSocietateLocalitate("societateLocalitate");
		companyBean.setSocietateJudet("societateJudet");
		companyBean.setSocietateCUI("societateCUI");
		companyBean.setSocietateNrInregRegCom("societateNrInregRegCom");

		companyBean.setFacturaNr("facturaNr");
		companyBean.setFacturaData(new Date());
		companyBean.setFacturaSumaDatorata(1234.34);

		companyBean.setContractNr("contractNr");
		companyBean.setContractData(new Date());

		companyBean.setAdresaInitialaNr("adresaInitialaNr");
		companyBean.setAdresaInitialaData(new Date());

		companyBean.setAdresaNouaNr("adresaNouaNr");
		companyBean.setAdresaNouaData(new Date());

		companyBean.setAnulFacturat("2012");
		companyBean.setJudecatoria("SECTORULUI 5");

		// companyBean.setIban("RO18CECEB31601RON2369073");
		// companyBean.setBanca("Banca CEC Bank");

		companyBean.setFacturaScadenta(new Date());

		GeneratorPdfSomatieJudecatorie instance = new GeneratorPdfSomatieJudecatorie(companyBean);
		FileUtils.writeBytesToFile(instance.renderPdf(), "/tmp/GeneratorPdfSomatieJudecatorieTst_1.pdf");
	}
}
