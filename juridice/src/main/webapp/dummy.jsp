<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>


<%
	/* Aceasta pagina este pentru a creea tag-uri pa care le vom copia in paginile noastre de facelets (XHTML) */
%>


<span style="font-weight: bold"></span>


		<t:outputLabel for="testPage" >
			<t:outputText value="Test page" />
		</t:outputLabel>

<h:form id="form2">
	<t:htmlTag value="h2">
		<t:outputText value="Test page" />
		<h:outputText value="Test page" escape="" />
	</t:htmlTag>
	<t:saveState value="#{examAssignedToUIBean}" />

	<t:messages errorClass="errorMsg" infoClass="infoMsg" showDetail="true" warnClass="errorMsg" layout="table"
		globalOnly="true" showSummary="false" />

	<t:message for="Text" errorClass="errorMsg" infoClass="infoMsg" tooltip="true" showSummary="false" showDetail="true"
		replaceIdWithLabel="true" />
<f:convertDateTime timeZone=""/>
		<f:convertDateTime pattern="dd.MM.yyyy" />

	<t:panelGrid cellpadding="0" cellspacing="1" id="table1" columns="2" 
	columnClasses="ColumnTop,ColumnTop" rendered="">
		<t:panelGroup colspan="2"/>
		<t:panelGroup />
		<t:commandButton value="Test" action="#{testUIBean.addMockData}"
			onclick="if ( ! confirm('Sunteti sigura')) return false" />

		<t:inputTextarea cols="30" rows="5" styleClass="" required="true" requiredMessage="" />
		<t:inputTextarea value="#{anularePachetCoduriUIBean.motivRespingere}" required="true" id="Motivul_Anularii">
			<f:validateLength maximum="200" />
		</t:inputTextarea>

	</t:panelGrid>

	<t:htmlTag value="br" />
	<t:htmlTag value="br" />
	<t:htmlTag value="br" />

	<t:selectOneMenu id="Nota" value="" required="true" />


	<t:selectOneMenu id="Nota" value="" required="true" readonly="true" disabled="true" rendered="">
		<f:selectItem itemValue="1" itemLabel="1" />
		<f:selectItem itemValue="2" itemLabel="2" />
		<f:selectItem itemValue="8" itemLabel="8" />
		<f:selectItem itemValue="9" itemLabel="9" />
		<f:selectItem itemValue="10" itemLabel="10" />
	</t:selectOneMenu>

	<t:graphicImage id="imagine_submit" style="border: 1px solid black;" url="/images/bar_code_47x18.gif"
		styleClass="formSubmitLeftmostImg" />

	<t:column>
		<f:facet name="header">
			<t:outputText value="data" style="font:Verdana" />
		</f:facet>
		<t:commandLink action="#{questionUIListBean.navEdit}" immediate="true"
			actionListener="#{searchUIListBean.markStatusAPrimitProforma}">
			<t:outputText value="#{dataItem.dateEntered}" />
			<t:updateActionListener property="#{questionUIBean.id}" value="#{dataItem.id}" />
			<t:updateActionListener property="#{questionUIBean.edit}" value="true" />
		</t:commandLink>
	</t:column>

	<t:panelGrid columns="2" style="font:Verdana" cellpadding="0">
		<t:outputText value="Telefon:" />
		<t:panelGrid columns="1">
			<t:outputText value="+4 021 317 80 31" />
			<t:outputText value="+4 021 317 80 32" />
			<t:outputText value="+4 021 317 80 33" />
		</t:panelGrid>
	</t:panelGrid>

	<t:panelGroup />
	<t:panelGroup colspan="2" rendered="">
		<t:message for="BlockMotivation" errorClass="errorMsg" infoClass="infoMsg" tooltip="true" showSummary="false"
			showDetail="true" replaceIdWithLabel="true" />
	</t:panelGroup>

	<t:outputLabel for="BlockMotivation" value="#{bundle.client_blocked_TextareaBlockMotivation}" />
	<t:inputTextarea value="#{clientUIBean.blockMessage}" rows="5" cols="80" required="true" id="BlockMotivation"
		styleClass="formField">
	</t:inputTextarea>
	<t:inputText maxlength="" size=""/>

	<t:selectOneRadio id="aprobare_termeni" required="true" value="asd" layout="lineDirection">
		<f:selectItem itemValue="Da" itemLabel="Da" />
		<f:selectItem itemValue="Nu" itemLabel="Nu" />
	</t:selectOneRadio>


	<t:graphicImage url="" rendered="" />

	<t:collapsiblePanel>
		<t:selectOneRadio id="aprobare_termeni" required="true" value="asd" layout="lineDirection">
			<f:selectItem itemValue="Da" itemLabel="Da" />
			<f:selectItem itemValue="Nu" itemLabel="Nu" />
		</t:selectOneRadio>
	</t:collapsiblePanel>


	<t:commandButton value="Cauta" styleClass="formButton" action="" />
	<t:commandButton value="&lt;&lt; Intrebarea Anterioara" styleClass="formButton" disabled="" rendered=""
		action="#{examAssignedToUIBean.eventNextQuestion}" />

	<t:dataTable rowIndexVar="indexVar" renderedIfEmpty="false" rendered=""></t:dataTable>


	<t:outputText value="#{dataItem.dateEntered}">
		<f:convertDateTime pattern="dd/MMM/yyyy" />
		<f:converter converterId="NullableLongConvertor"/>
	</t:outputText>

	<t:div styleClass="#{examAssignedToDetailsUIBean.currentQuestion.optionValue2  ? 'question_true' : 'question_true'}">
	</t:div>

	<f:selectItem itemLabel="Nici un filtru" itemValue="" />


	<t:commandButton value="Cauta" styleClass="formButton" action="" />
	<t:commandLink action="navBack" immediate="true">
		<t:outputText value="&amp;lt;&amp;lt; Inapoi" escape="false" />
	</t:commandLink>

	<t:graphicImage url="/images/empty.gif" width="120" height="1" />


	<t:inputCalendar id="dateArhivariiDoc" monthYearRowClass="yearMonthHeader" 
		weekRowClass="weekHeader" currentDayCellClass="currentDayCell" maxlength="10"
		value="#{documentSubClassUIBean.dataItem.dateArhivariiDoc}" renderAsPopup="true"
		styleClass="formField" popupTodayString="Astazi" 
		popupWeekString="Saptamana" addResources="true"
		renderPopupButtonAsImage="true" popupSelectDateMessage="Data" 
		popupDateFormat="dd.MM.yyyy" size="10" disabled="true">
		<f:convertDateTime timeZone="#{tzUIBean.tz}" pattern="dd.MM.yyyy" />
	</t:inputCalendar>



	<t:dataTable value="#{curentTokClientGtinsListUIBean.dataItems}" var="dataItem" border="0" cellspacing="1"
		cellpadding="1" headerClass="tableHeader" rowClasses="rowOdd,rowEven" rowIndexVar="indexVar" renderedIfEmpty="false">

		<t:column>
			<f:facet name="header">
				<t:outputText value="Nr. crt." />
			</f:facet>
			<t:outputText value="#{indexVar + 1}" />
		</t:column>

		<t:column>
			<f:facet name="header">
				<t:outputText value="Data cererii" />
			</f:facet>
			<t:outputText value="#{dataItem.dataTrimitere}" />
		</t:column>

		<t:column>
			<f:facet name="header">
				<t:outputText value="Modifica" />
			</f:facet>
			<t:commandLink action="ok-edit" immediate="true" onclick="if ( ! confirm('')) return false">
				<t:graphicImage url="../images/modifica.gif" />
				<t:updateActionListener property="#{curentTokClientGtinsUIBean.id}" value="#{dataItem.id}" />
				<t:updateActionListener property="#{curentTokClientGtinsUIBean.edit}" value="true" />
			</t:commandLink>
		</t:column>

	</t:dataTable>

    <t:dataList value="#{dataItem.intervalIps}" var="intervalIp" layout="simple">
        <t:outputText value="#{intervalIp.intervalStart} - #{intervalIp.intervalStop}"/>
    </t:dataList>

				<t:inputText id="Email" value="#{signUpUIBean.dataItem.email}" 
					maxlength="70" required="true" styleClass="formField">
					<t:validateEmail />
				</t:inputText>

			<h:outputLink value="#{facesContext.externalContext.requestContextPath}/titular_drept_dispozitie/arhivare_manuala_selectie_subclasa.jsf">
				<t:outputText value="Adaugare document nou" />
			</h:outputLink>


<t:selectBooleanCheckbox  id="Detalii_Companie_Activitate_Info_EPC"
	value="#{serviceProviderCompanyUIBean.dataItem.companieNeedInformation}" label=""/>

		<t:inputSecret id="password" styleClass="formField" required="true" value="#{utilizatorUIBean.dataItem.password}"
			maxlength="255" redisplay="true">
			<f:validateLength minimum="6" />
		</t:inputSecret>

<t:selectOneListbox ></t:selectOneListbox>
<f:validateDoubleRange minimum="0" maximum="10000000" />
<f:validateLongRange minimum="0" />

					<h:inputText id="refferedInvoiceNumber" styleClass="formField"  readonly="true"
						value="#{nonEdiMesajRendererRealOrderToInvoicUiBean.invoic.refferedInvoiceNumber}" />

<t:panelTabbedPane selectedIndex="0" id="fileTabs" width="800px"  
    activeSubStyleClass="fileTabContent">  
    <t:panelTab label="General Information">  
        <h:panelGrid columns="3">  
etc.  
        </h:panelGrid>  
    </t:panelTab>  
    <t:panelTab label="General Information">  
        This is the stuff that goes on tab 2  
    </t:panelTab>  
    <t:panelTab label="General Information">  
        This is the stuff that goes on tab 3  
    </t:panelTab>  
</t:panelTabbedPane> 

<t:graphicImage url="/images/Button-Blank-Yellow-icon-24.png" 
	border="0" 
	alt="Incomplete Transaction, Status not 'paid'" 
	title="Incomplete Transaction, Status not 'paid'"
	rendered="#{item.onlineIncompleteTransacton}" 
	/>

<t:dataList layout="" />

</h:form>

