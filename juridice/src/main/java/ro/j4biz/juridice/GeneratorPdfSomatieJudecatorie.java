package ro.j4biz.juridice;

import ro.j4biz.commons.fop.render.FoBlock;
import ro.j4biz.commons.fop.render.FoFlow;
import ro.j4biz.commons.fop.render.FoSimplePageMaster;

public class GeneratorPdfSomatieJudecatorie extends AbstractGeneratorPdf {

	private String body_0 = "JUDECATORIA {17}";

	//
	private String body_1_dl_prez = "DOMNULE PRESEDINTE,";
	//

	private String body_2_1 = "Subscrisa ";
	private String body_2_2 = "Asociatia GS1 ROMANIA";
	private String body_2_3 = ", cu sediul în Bucuresti, str. Louis Blank nr. 1, parter + et. 1, Sector 1, "
					+ "având cod fiscal 5659755, cont bancar RO78RNCB0072049674220001, deschis la Banca Comerciala Româna, "
					+ "Sucursala sector 1, reprezentata legal prin presedinte Iosep Ciprian, în calitate de ";
	private String body_2_4 = "creditoare,";

	private String body_3_1 = "în contradictoriu cu ";
	private String body_3_2 = "debitoarea {0} ";
	private String body_3_3 = "cu sediul social în {1}, {2}, {3}, {4}, "
					+ "având Numar de Ordine în Registrul Comertului {6} si Cod Unic de Înregistrare {5}, "
	// + "cont bancar {18}, deschis la {19},"
	;

	private String body_4 = "formulam prezenta,";

	private String body_5 = "CERERE DE CHEMARE ÎN JUDECATA";

	private String body_6 = "prin intermediul careia solicitam ca prin hotarârea ce o veti pronunta "
					+ "sa dispuneti emiterea unei ordonante de plata pentru obligarea debitoarei {0} "
					+ "la plata sumei de {9} lei reprezentând debit neachitat.";

	private String body_7_1 = "În fapt, ";
	private String body_7_2 = "la data de {11}, între societatea noastra si debitoarea ";
	private String body_7_3 = "{0}";
	private String body_7_4 = "a fost încheiat contractul de utilizare a sistemului GS1 nr. {10} "
					+ "prin care aceasta primea dreptul de utilizare a codurilor EAN si a sistemului de comunicatie EANCOM.";

	private String body_8_1 = "Potrivit dispozitiilor art. 6 din contractul mai sus mentionat, semnat si stampilat de catre ambele parti, societatea ";
	private String body_8_2 = "{0} ";
	private String body_8_3 = "s-a obligat sa achite cotizatia anuala în prima luna a fiecarui an, "
					+ "contractul fiind încheiat pe o perioada nedeterminata.";

	//private String body_9 = "DOMNULUI PRESEDINTE AL JUDECATORIEI {17}";

	private String body_10 = "În baza acestui contract a fost emisa factura nr. {7}/{8} în cuantum total de {9} lei, "
					+ "scadenta în data de {20}, factura neachitata pâna în prezent de catre debitoare.";

	private String body_11 = "Creanta mai sus mentionata îndeplineste conditiile prevazute de dispozitiile art. 1.013 "
					+ "din Codul de procedura civila, fiind o creanta certa, lichida si exigibila, "
					+ "rezultata dintr-un contract civil încheiat între profesionisti.";

	private String body_12 = "Mentionam faptul ca debitoarea a fost somata conform dispozitiilor art. 1.014 "
					+ "din Codul de procedura civila, însa aceasta nu a dat curs solicitarii noastre.";

	private String body_13 = "Pentru motivele mai sus mentionate va solicitam sa admiteti cererea "
					+ "si sa dispuneti emiterea unei ordonante de plata pentru obligarea debitoarei {0} "
					+ "la plata sumei de {9} lei reprezentând debit neachitat.";

	private String body_14 = "În temeiul art. 453 alin. (1) din Codul de procedura civila solicitam obligarea debitoarei la plata cheltuielilor de judecata.";

	private String body_15_1 = "În drept, ";
	private String body_15_2 = "ne întemeiem prezenta pe dispozitiile art. 453 alin. (1) si art. 1.013 – 1.024 din Codul de procedura civila.";

	private String body_16_1 = "În dovedirea prezentei ";
	private String body_16_2 = "întelegem sa ne folosim de proba cu încrisuri.";

	private String body_17 = "Anexam urmatoarele încrisuri în copie conforma cu originalul: ";

	private String body_18_1 = " - Contractul de utilizare a sistemului GS1  nr. {10}/{11};";
	private String body_18_2 = " - Factura nr. {7}/{8};";
	private String body_18_3 = " - Dovada de comunicare a facturii catre debitoare;";
	private String body_18_4 = " - Dovada comunicarii somatiei catre debitoare prevazuta la art. 1014 alin. (1) "
					+ "din Codul de procedura civila;";
	private String body_18_5 = " - Hotarârea Adunarii Generale privind nivelul cotizatiei pentru anul {16};";
	private String body_18_6 = " - Dovada de achitare a taxei judiciare de timbru în cuantum de 200 lei – în original.";

	private String body_19 = "             Data:                                               "
					+ "          Asociatia GS1 ROMANIA,";
	private String body_20 = "                                                                 "
					+ "            prin Presedinte dl. Ciprian Iosep";
	private String body_21 = "                                                                 "
					+ "            __________________________";

	private String body_22 = "DOMNULUI PRESEDINTE AL JUDECATORIEI {17}";

	public GeneratorPdfSomatieJudecatorie(final CompanyBean companyBean) {
		super(companyBean);
	}

	@Override
	public byte[] renderPdf() throws Exception {
		if (bytes == null) {
			getPageContent().add(newBlock(body_0).setFontWeight("bold"));

			getPageContent().add(new FoBlock().setSpaceBefore("20mm"));
			getPageContent().add(newBlock(body_1_dl_prez).setFontWeight("bold").setTextAlign("center"));

			getPageContent().add(new FoBlock().setSpaceBefore("10mm"));

			{
				FoBlock block = new FoBlock();
				block.add(newInline(prepareText(body_2_1)));
				block.add(newInline(prepareText(body_2_2)).setFontWeight("bold"));
				block.add(newInline(prepareText(body_2_3)));
				block.add(newInline(prepareText(body_2_4)).setFontWeight("bold"));
				getPageContent().add(block);
			}

			getPageContent().add(new FoBlock().setSpaceBefore("4mm"));

			{
				FoBlock block = new FoBlock();
				block.add(newInline(prepareText(body_3_1)));
				block.add(newInline(prepareText(body_3_2)).setFontWeight("bold"));
				block.add(newInline(prepareText(body_3_3)));
				getPageContent().add(block);
			}

			getPageContent().add(new FoBlock().setSpaceBefore("4mm"));
			getPageContent().add(newBlock(body_4).setFontStyle("italic"));

			getPageContent().add(new FoBlock().setSpaceBefore("4mm"));
			getPageContent().add(newBlock(body_5).setFontWeight("bold").setTextAlign("center"));

			getPageContent().add(new FoBlock().setSpaceBefore("4mm"));
			getPageContent().add(newBlock(body_6).setFontWeight("bold"));

			getPageContent().add(new FoBlock().setSpaceBefore("4mm"));
			{
				FoBlock block = new FoBlock();
				block.add(newInline(prepareText(body_7_1)).setFontWeight("bold"));
				block.add(newInline(prepareText(body_7_2)));
				block.add(newInline(prepareText(body_7_3)).setFontWeight("bold"));
				block.add(newInline(prepareText(body_7_4)));
				getPageContent().add(block);
			}

			getPageContent().add(new FoBlock().setSpaceBefore("4mm"));
			{
				FoBlock block = new FoBlock();
				block.add(newInline(prepareText(body_8_1)));
				block.add(newInline(prepareText(body_8_2)).setFontWeight("bold"));
				block.add(newInline(prepareText(body_8_3)));
				getPageContent().add(block);
			}

//			getPageContent().add(new FoBlock().setSpaceBefore("10mm"));
//			getPageContent().add(newBlock(body_9).setFontWeight("bold").setTextAlign("center"));

			getPageContent().add(new FoBlock().setSpaceBefore("5mm"));
			getPageContent().add(newBlock(body_10));

			getPageContent().add(new FoBlock().setSpaceBefore("4mm"));
			getPageContent().add(newBlock(body_11));

			getPageContent().add(new FoBlock().setSpaceBefore("4mm"));
			getPageContent().add(newBlock(body_12));

			getPageContent().add(new FoBlock().setSpaceBefore("4mm"));
			getPageContent().add(newBlock(body_13).setFontWeight("bold"));

			getPageContent().add(new FoBlock().setSpaceBefore("4mm"));
			getPageContent().add(newBlock(body_14));

			getPageContent().add(new FoBlock().setSpaceBefore("4mm"));
			{
				FoBlock block = new FoBlock();
				block.add(newInline(prepareText(body_15_1)).setFontWeight("bold"));
				block.add(newInline(prepareText(body_15_2)));
				getPageContent().add(block);
			}

			getPageContent().add(new FoBlock().setSpaceBefore("4mm"));
			{
				FoBlock block = new FoBlock();
				block.add(newInline(prepareText(body_16_1)).setFontWeight("bold"));
				block.add(newInline(prepareText(body_16_2)));
				getPageContent().add(block);
			}

			getPageContent().add(new FoBlock().setSpaceBefore("4mm"));
			getPageContent().add(newBlock(body_17).setFontWeight("bold"));

			getPageContent().add(new FoBlock().setSpaceBefore("4mm"));
			getPageContent().add(newBlock(body_18_1));
			getPageContent().add(newBlock(body_18_2));
			getPageContent().add(newBlock(body_18_3));
			getPageContent().add(newBlock(body_18_4));
			getPageContent().add(newBlock(body_18_5));
			getPageContent().add(newBlock(body_18_6));

			getPageContent().add(new FoBlock().setSpaceBefore("15mm"));
			getPageContent().add(newBlock(body_19).setFontWeight("bold").setWhiteSpaceCollapse("false"));
			getPageContent().add(newBlock(body_20).setWhiteSpaceCollapse("false"));
			getPageContent().add(new FoBlock().setSpaceBefore("15mm"));
			getPageContent().add(newBlock(body_21).setWhiteSpaceCollapse("false"));

			getPageContent().add(new FoBlock().setSpaceBefore("40mm"));
			getPageContent().add(newBlock(body_22).setFontWeight("bold"));

			bytes = doc.getPdf();

		}
		return bytes;
	}

	@Override
	protected FoFlow getPageContent() throws Exception {
		if (pageContent == null) {
			// A4 portrait.
			super.getPageContent();

			FoSimplePageMaster simplePageMaster = getDoc().getLayoutMasterSet().getSimplePageMaster();
			// suprascriem marginile
			simplePageMaster.setMarginTop("1in").setMarginBottom("1in").setMarginLeft("1in").setMarginRight("1in");

			pageContent = getDoc().getPageSequence().getPageContent();
		}
		return pageContent;
	}
}
