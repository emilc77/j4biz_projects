package ro.j4biz.juridice;

import org.w3c.dom.Document;
import ro.j4biz.commons.core.MessageFormatUtils;
import ro.j4biz.commons.core.StringUtils;
import ro.j4biz.commons.core.io.FileUtils;
import ro.j4biz.commons.fop.render.AbstractFo;
import ro.j4biz.commons.fop.render.FoBlock;
import ro.j4biz.commons.fop.render.FoDocument;
import ro.j4biz.commons.fop.render.FoFlow;
import ro.j4biz.commons.fop.render.FoInline;
import ro.j4biz.commons.fop.render.table.FoTableCell;

import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public abstract class AbstractGeneratorPdf {
	private final Object[] tokensOfInfo;

	protected FoFlow pageContent;
	protected FoDocument doc;
	protected byte[] bytes;

	protected static final String defaultFontSize = "11pt";
	protected static final String defaultBorder = "0.3pt solid #000000";
	protected static final String defaultFontFamily = "Verdana";

	public AbstractGeneratorPdf(final CompanyBean companyBean) {
		// this.companyBean = companyBean;

		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		DecimalFormat df = new DecimalFormat("####################0.##");

		tokensOfInfo = new Object[21];
		tokensOfInfo[0] = companyBean.getSocietateNume();
		tokensOfInfo[1] = companyBean.getSocietateStradaNr();
		tokensOfInfo[2] = companyBean.getSocietateCodPostal();
		tokensOfInfo[3] = companyBean.getSocietateLocalitate();
		tokensOfInfo[4] = companyBean.getSocietateJudet();
		tokensOfInfo[5] = companyBean.getSocietateCUI();
		tokensOfInfo[6] = companyBean.getSocietateNrInregRegCom();

		tokensOfInfo[7] = companyBean.getFacturaNr();
		tokensOfInfo[8] = sdf.format(companyBean.getFacturaData());
		tokensOfInfo[9] = df.format(companyBean.getFacturaSumaDatorata());

		tokensOfInfo[10] = companyBean.getContractNr();
		tokensOfInfo[11] = sdf.format(companyBean.getContractData());

		// s-a intamplat sa nu avem aceasta adresa initiala, de aceea am pus optional
		if (StringUtils.isEmpty(companyBean.getAdresaInitialaNr())) {
			tokensOfInfo[12] = "............";
		} else {
			tokensOfInfo[12] = companyBean.getAdresaInitialaNr();
		}

		if (companyBean.getAdresaInitialaData() == null) {
			tokensOfInfo[13] = ".............";
		} else {
			tokensOfInfo[13] = sdf.format(companyBean.getAdresaInitialaData());
		}

		//
		if (StringUtils.isEmpty(companyBean.getAdresaNouaNr())) {
			tokensOfInfo[14] = "..............";
		} else {
			tokensOfInfo[14] = companyBean.getAdresaNouaNr();
		}
		if (companyBean.getAdresaNouaData() == null) {
			tokensOfInfo[15] = "..............";
		} else {
			tokensOfInfo[15] = sdf.format(companyBean.getAdresaNouaData());
		}

		tokensOfInfo[16] = companyBean.getAnulFacturat();

		if (companyBean.getJudecatoria() != null) {
			tokensOfInfo[17] = companyBean.getJudecatoria().toUpperCase();
		}

		tokensOfInfo[18] = "";// companyBean.getIban();
		tokensOfInfo[19] = "";// companyBean.getBanca();

		if (companyBean.getFacturaScadenta() != null) {
			tokensOfInfo[20] = sdf.format(companyBean.getFacturaScadenta());
		}
	}

	public FoDocument getDoc() {
		if (doc == null) {
			doc = new FoDocument();
		}
		return doc;
	}

	// metoda aceasta este doar ca sa fac asserturi pe XML-ul rezultat.
	public Document getFoDocument() throws Exception {
		// asta ca sa faca calcularea PDF-ului.
		renderPdf();

		return doc.getDocument();
	}

	public abstract byte[] renderPdf() throws Exception;

	protected FoTableCell newBorderedFoTableCell() {
		FoTableCell cell1 = new FoTableCell();
		cell1.setBorderBottom(defaultBorder).setBorderLeft(defaultBorder).setBorderRight(defaultBorder)
						.setBorderTop(defaultBorder).setPaddingTop("8pt").setPaddingBottom("5pt")
						.setPaddingRight("5pt").setPaddingLeft("5pt");
		return cell1;
	}

	protected void addMultilineText(final AbstractFo cell, final String text) throws IOException {
		addMultilineText(cell, text, null, null, null);
	}

	protected void addMultilineText(
					final AbstractFo cell,
					final String text,
					final String fontStyle,
					final String fontWeight,
					final String fontSize) throws IOException {

		List<String> contentLines = FileUtils.getContentLines(new StringReader(text));
		for (String line : contentLines) {
			// Times
			FoBlock blk = newBlock(line, fontSize);
			if (!StringUtils.isEmpty(fontStyle)) {
				blk.setFontStyle(fontStyle);
			}
			if (!StringUtils.isEmpty(fontWeight)) {
				blk.setFontWeight(fontWeight);
			}

			cell.add(blk);
		}
	}

	protected FoInline newInline(final String text) {
		return newInline(text, defaultFontSize);
	}

	protected FoInline newInline(final String text, final String fontSize) {
		return new FoInline(text).setFontSize(fontSize).setFontFamily(defaultFontFamily);
	}

	protected FoBlock newBlock(final String text) {
		return newBlock(text, defaultFontSize);
	}

	protected FoBlock newBlock(final String text, final String fontSize) {
		FoBlock blk = new FoBlock(prepareText(text)).setFontSize(defaultFontSize).setFontFamily(defaultFontFamily);

		if (!StringUtils.isEmpty(fontSize)) {
			blk.setFontSize(fontSize);
		}

		return blk;
	}

	protected String prepareText(final String text) {
		return MessageFormatUtils.getMessage(null, text, text, tokensOfInfo);
	}

	protected FoFlow getPageContent() throws Exception {
		if (pageContent == null) {
			getDoc().setA4PortraitDefaults();
			pageContent = getDoc().getPageSequence().getPageContent();
		}
		return pageContent;
	}

	//
	// protected String double2String(final double value) {
	// return getDecimalFormat().format(value);
	// }
	//
	// protected String double2String(final Double value) {
	// if (value == null) {
	// return "";
	// }
	// return getDecimalFormat().format(value.doubleValue());
	// }

}
