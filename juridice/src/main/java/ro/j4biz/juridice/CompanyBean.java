package ro.j4biz.juridice;

import java.io.Serializable;
import java.util.Date;

public class CompanyBean implements Serializable {
	private String societateNume;
	private String societateStradaNr;
	private String societateCodPostal;
	private String societateLocalitate;
	private String societateJudet;
	private String societateCUI;
	private String societateNrInregRegCom;

	private String facturaNr;
	private Date facturaData;
	private double facturaSumaDatorata;

	private String contractNr;
	private Date contractData;

	private String adresaInitialaNr;
	private Date adresaInitialaData;

	private String adresaNouaNr;
	private Date adresaNouaData;

	private String anulFacturat;
	private String judecatoria;

	// private String iban;
	// private String banca;
	private Date facturaScadenta;

	public String getSocietateNume() {
		return societateNume;
	}

	public String getSocietateStradaNr() {
		return societateStradaNr;
	}

	public String getSocietateCodPostal() {
		return societateCodPostal;
	}

	public String getSocietateLocalitate() {
		return societateLocalitate;
	}

	public String getSocietateJudet() {
		return societateJudet;
	}

	public String getSocietateCUI() {
		return societateCUI;
	}

	public String getSocietateNrInregRegCom() {
		return societateNrInregRegCom;
	}

	public String getFacturaNr() {
		return facturaNr;
	}

	public Date getFacturaData() {
		return facturaData;
	}

	public double getFacturaSumaDatorata() {
		return facturaSumaDatorata;
	}

	public String getContractNr() {
		return contractNr;
	}

	public Date getContractData() {
		return contractData;
	}

	public String getAdresaInitialaNr() {
		return adresaInitialaNr;
	}

	public Date getAdresaInitialaData() {
		return adresaInitialaData;
	}

	public String getAdresaNouaNr() {
		return adresaNouaNr;
	}

	public Date getAdresaNouaData() {
		return adresaNouaData;
	}

	public void setSocietateNume(final String societateNume) {
		this.societateNume = societateNume;
	}

	public void setSocietateStradaNr(final String societateStradaNr) {
		this.societateStradaNr = societateStradaNr;
	}

	public void setSocietateCodPostal(final String societateCodPostal) {
		this.societateCodPostal = societateCodPostal;
	}

	public void setSocietateLocalitate(final String societateLocalitate) {
		this.societateLocalitate = societateLocalitate;
	}

	public void setSocietateJudet(final String societateJudet) {
		this.societateJudet = societateJudet;
	}

	public void setSocietateCUI(final String societateCUI) {
		this.societateCUI = societateCUI;
	}

	public void setSocietateNrInregRegCom(final String societateNrInregRegCom) {
		this.societateNrInregRegCom = societateNrInregRegCom;
	}

	public void setFacturaNr(final String facturaNr) {
		this.facturaNr = facturaNr;
	}

	public void setFacturaData(final Date facturaData) {
		this.facturaData = facturaData;
	}

	public void setFacturaSumaDatorata(final double facturaSumaDatorata) {
		this.facturaSumaDatorata = facturaSumaDatorata;
	}

	public void setContractNr(final String contractNr) {
		this.contractNr = contractNr;
	}

	public void setContractData(final Date contractData) {
		this.contractData = contractData;
	}

	public void setAdresaInitialaNr(final String adresaInitialaNr) {
		this.adresaInitialaNr = adresaInitialaNr;
	}

	public void setAdresaInitialaData(final Date adresaInitialaData) {
		this.adresaInitialaData = adresaInitialaData;
	}

	public void setAdresaNouaNr(final String adresaNouaNr) {
		this.adresaNouaNr = adresaNouaNr;
	}

	public void setAdresaNouaData(final Date adresaNouaData) {
		this.adresaNouaData = adresaNouaData;
	}

	public String getAnulFacturat() {
		return anulFacturat;
	}

	public void setAnulFacturat(final String anulFacturat) {
		this.anulFacturat = anulFacturat;
	}

	public String getJudecatoria() {
		return judecatoria;
	}

	public void setJudecatoria(final String judecatoria) {
		this.judecatoria = judecatoria;
	}

	// public String getIban() {
	// return iban;
	// }
	//
	// public String getBanca() {
	// return banca;
	// }
	//
	// public void setIban(final String iban) {
	// this.iban = iban;
	// }
	//
	// public void setBanca(final String banca) {
	// this.banca = banca;
	// }

	public Date getFacturaScadenta() {
		return facturaScadenta;
	}

	public void setFacturaScadenta(final Date facturaScadenta) {
		this.facturaScadenta = facturaScadenta;
	}
}
