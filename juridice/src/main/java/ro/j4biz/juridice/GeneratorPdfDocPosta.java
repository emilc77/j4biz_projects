package ro.j4biz.juridice;

import ro.j4biz.commons.fop.render.FoBlock;

public class GeneratorPdfDocPosta extends AbstractGeneratorPdf {
	private String body_1_ofbuc = "/OFBUC13                                                              VA .................";
	private String body_2_nota_inventar = "NOTA DE INVENTAR";

	private String body_3_destinar = "DESTINAR";
	// private String body_3_puncte = "..............................................";
	private String body_4_adresa = "ADRESA";
	private String body_5_localitatea = "LOCALITATEA";
	private String body_6_continut = "CONTINUT:";
	private String body_7_continut_linii = "1. ADRESA NR.: {14}/{15}\n"
	//
					+ "2. CONTRACT {10}/{11}\n"
					//
					+ "3. ADRESA NR. {12}/{13}\n"
					//
					+ "4. FACTURA {7}/{8}\n"
					//
					+ "5. ..............\n"
					//
					+ "6. ....................";

	private String body_8_valoare = "VALOARE 20 LEI";
	private String body_9_expeditor = "EXPEDITOR";
	private String body_10_gs1 = "ASOCIATIA GS1 ROMANIA";
	private String body_11_adresa = "ADRESA: STR. LOUIS BLANK, NR.1, PARTER";
	private String body_12_localitate = "LOCALITATE: BUCURESTI, SECTOR 1";
	private String body_13_stampila = "STAMPILA CU DATA PREZENTARII                    SEMNATURA LUCRATORULUI POSTAL";

	public GeneratorPdfDocPosta(final CompanyBean companyBean) {
		super(companyBean);
	}

	@Override
	public byte[] renderPdf() throws Exception {
		if (bytes == null) {

			getPageContent().add(newBlock(body_1_ofbuc).setWhiteSpaceCollapse("false"));

			getPageContent().add(new FoBlock().setSpaceBefore("10mm"));

			getPageContent().add(newBlock(body_2_nota_inventar).setTextAlign("center"));

			getPageContent().add(new FoBlock().setSpaceBefore("10mm"));

			getPageContent().add(newBlock(body_3_destinar));
			getPageContent().add(newBlock("{0}"));

			getPageContent().add(new FoBlock().setSpaceBefore("10mm"));

			getPageContent().add(newBlock(body_4_adresa));
			getPageContent().add(newBlock("{1}, {2}"));

			getPageContent().add(new FoBlock().setSpaceBefore("10mm"));

			getPageContent().add(newBlock(body_5_localitatea));
			getPageContent().add(newBlock("{3}, jud. {4}"));

			getPageContent().add(new FoBlock().setSpaceBefore("10mm"));

			getPageContent().add(newBlock(body_6_continut));
			addMultilineText(getPageContent(), body_7_continut_linii);

			getPageContent().add(new FoBlock().setSpaceBefore("10mm"));

			getPageContent().add(newBlock(body_8_valoare));

			getPageContent().add(new FoBlock().setSpaceBefore("20mm"));

			getPageContent().add(newBlock(body_9_expeditor));

			getPageContent().add(new FoBlock().setSpaceBefore("10mm"));

			getPageContent().add(newBlock(body_10_gs1));
			getPageContent().add(newBlock(body_11_adresa));
			getPageContent().add(newBlock(body_12_localitate));

			getPageContent().add(new FoBlock().setSpaceBefore("10mm"));

			getPageContent().add(newBlock(body_13_stampila).setWhiteSpaceCollapse("false"));

			bytes = doc.getPdf();

		}
		return bytes;

	}

}
