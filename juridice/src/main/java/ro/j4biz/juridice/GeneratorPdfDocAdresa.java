package ro.j4biz.juridice;

import ro.j4biz.commons.core.StringUtils;
import ro.j4biz.commons.core.io.FileUtils;
import ro.j4biz.commons.fop.render.FoBlock;
import ro.j4biz.commons.fop.render.FoExternalGraphic;
import ro.j4biz.commons.fop.render.FoFlow;
import ro.j4biz.commons.fop.render.FoLayoutMasterSet;
import ro.j4biz.commons.fop.render.FoPageSequence;
import ro.j4biz.commons.fop.render.FoRegionAfter;
import ro.j4biz.commons.fop.render.FoRegionBefore;
import ro.j4biz.commons.fop.render.FoRegionBody;
import ro.j4biz.commons.fop.render.FoRegionEnd;
import ro.j4biz.commons.fop.render.FoRegionStart;
import ro.j4biz.commons.fop.render.FoSimplePageMaster;
import ro.j4biz.commons.fop.render.FoStaticContent;
import ro.j4biz.commons.fop.render.table.FoTable;
import ro.j4biz.commons.fop.render.table.FoTableBody;
import ro.j4biz.commons.fop.render.table.FoTableCell;
import ro.j4biz.commons.fop.render.table.FoTableColumn;
import ro.j4biz.commons.fop.render.table.FoTableRow;

import java.io.File;
import java.io.IOException;

public class GeneratorPdfDocAdresa extends AbstractGeneratorPdf {

	private String headerText = "GS1 ROMANIA - ASOCIATIA NATIONALA DE CODIFICARE A ARTICOLELOR IN SISTEM GS1\n"
					+ "Str. Louis Blank nr. 1, parter + et. 1, Sector 1, Cod 011751, Bucuresti, Romania\n"
					+ "Tel.: +4021 317 80 31, +4021 317 80 32 - Fax: +4021 317 80 33\n"
					+ "E-mail: office@gs1.ro - Web site: www.gs1.ro";

	private String footerText = "GS1 - The Global Language of Business";

	// Strings in the body
	private String body_1_nr = "Nr. {14}/{15}";
	private String body_2_catre = "Catre:\n"
	//
					+ "{0}\n"
					//
					+ "Sediu social: {1}, {2}, {3}, {4}\n"
					//
					+ "CUI: {5}\n"
					//
					+ "{6}";

	private String body_3_subscrisa = "Subscrisa Asociatia GS1 ROMANIA, organizatie nationala de numerotare a produselor si locatiilor, "
					+ "persoana juridica româna, inregistrata in Registrul Asociatiilor si Fundatiilor sub nr. 196/23.02.2000, "
					+ "cu sediul social in Bucuresti, str. Louis Blank, nr. 1, parter + et. 1, Sector 1, "
					+ "cod postal 011751, având Cod Unic de inregistrare RO 5659755, "
					+ "reprezentata prin Presedinte dl. Ciprian Iosep,";
	private String body_4_in_temeiul = "in temeiul art. 1014 alin. (1) din Codul de procedura civila,";
	private String body_5_va_somam = "VA SOMAM";
	private String body_6_ca15zile = "ca in temen de 15 zile de la receptionarea prezentei sa procedati la achitarea sumei de {9} lei, "
					+ "reprezentând contravaloarea facturii nr. {7} din data de {8}.";
	private String body_7_prinContractul = "Prin Contractul de utilizare nr. {10} incheiat la data de {11}"
					+ " societatea dumneavoastra a aderat la sistemul international de standarde GS1, "
					+ "devenind astfel membru GS1 România in scopul codificarii EAN/GS1 "
					+ "si a reprezentarii prin cod de bare EAN-8, EAN-13, IFT-14, GS1 128, GS1 dataBar, "
					+ "GS1 DataMatrix pentru identificarea automata la punctele de vânzare/ logistica din sistemele deschise de comert.";
	private String body_8_potrivit = "Potrivit dispozitiilor art. 6 din Contractul de utilizare mai sus mentionat, "
					+ "societatea dumneavoastra s-a obligat sa achite catre subscrisa taxa anuala de licenta GS1 (cotizatia anuala) "
					+ "in prima luna a fiecarui an.";
	private String body_9_inAcestSens = "In acest sens, prin Adresa nr. {12}/{13} va fost remisa factura nr {7}. din data de {8} "
					+ "in cuantum de {9} lei reprezentând contravaloarea taxei de licenta GS1 aferenta anului {16}, "
					+ "factura neachitata pâna la data prezentei. ";
	private String body_10_inSituatia = "In situatia in care nu veti da curs prezentei somatii, "
					+ "ne rezervam dreptul de a formula toate demersurile legale impotriva societatii dumneavoastra "
					+ "pentru recuperarea sumei mai sus mentionate, "
					+ "la aceasta urmand a se adauga dobânda de referinta stabilita de Banca Nationala a României, "
					+ "precum si cheltuielile de judecata constand in onorariu de avocat, taxa judiciara de timbru, "
					+ "timbru judiciar si eventualele cheltuieli de executare silita.";
	private String body_11_Anexam = "Anexam prezentei urmatoarele documente:";
	private String body_12_Contractul = "- Contractul de utilizare nr. {10} din data de {11};\n"
	//
					+ "- Adresa nr. {12}/{13};\n"
					//
					+ "- Factura nr. {7}. din data de {8} in cuantum de {9} lei .";

	private String body_13_CuStima = "Cu stima,\n" + "Presedinte GS1 Romania\n" + "Ciprian Iosep";

	public GeneratorPdfDocAdresa(final CompanyBean companyBean) {
		super(companyBean);
	}

	@Override
	public byte[] renderPdf() throws Exception {
		if (bytes == null) {
			// getPageContent().add(new FoBlock().setFontSize("9pt"));
			getPageContent().add(new FoBlock().setSpaceBefore("7mm"));

			getPageContent().add(newBlock(body_1_nr).setTextAlign("right").setFontWeight("bold"));

			getPageContent().add(new FoBlock().setSpaceBefore("2mm"));

			addMultilineText(getPageContent(), body_2_catre, null, "bold", null);

			getPageContent().add(new FoBlock().setSpaceBefore("10mm"));

			getPageContent().add(newBlock(body_3_subscrisa));

			getPageContent().add(new FoBlock().setSpaceBefore("3mm"));

			getPageContent().add(newBlock(body_4_in_temeiul).setFontWeight("bold").setTextAlign("center"));
			getPageContent().add(new FoBlock().setSpaceBefore("3mm"));
			getPageContent().add(newBlock(body_5_va_somam).setFontWeight("bold").setTextAlign("center"));
			getPageContent().add(new FoBlock().setSpaceBefore("3mm"));
			getPageContent().add(newBlock(body_6_ca15zile).setFontWeight("bold"));
			getPageContent().add(new FoBlock().setSpaceBefore("3mm"));

			getPageContent().add(newBlock(body_7_prinContractul));
			getPageContent().add(new FoBlock().setSpaceBefore("3mm"));
			getPageContent().add(newBlock(body_8_potrivit));
			getPageContent().add(new FoBlock().setSpaceBefore("3mm"));
			getPageContent().add(newBlock(body_9_inAcestSens));
			getPageContent().add(new FoBlock().setSpaceBefore("3mm"));
			getPageContent().add(newBlock(body_10_inSituatia).setFontWeight("bold"));
			getPageContent().add(new FoBlock().setSpaceBefore("3mm"));

			getPageContent().add(newBlock(body_11_Anexam));
			addMultilineText(getPageContent(), body_12_Contractul);
			getPageContent().add(new FoBlock().setSpaceBefore("3mm"));
			addMultilineText(getPageContent(), body_13_CuStima);

			bytes = doc.getPdf();

		}
		return bytes;
	}

	@Override
	protected FoFlow getPageContent() throws IOException {
		if (pageContent == null) {
			FoLayoutMasterSet layoutMasterSet = getDoc().getLayoutMasterSet();

			// <fo:simple-page-master master-name="simple" page-height="11in"
			// page-width="8.5in" margin-top="0.75in" margin-bottom="0.5in"
			// margin-left="0.75in" margin-right="0.5in">
			FoSimplePageMaster simplePageMaster = layoutMasterSet.getSimplePageMaster();
			simplePageMaster.setMasterName("simple").setPageHeight("11in").setPageWidth("8.5in").setMarginTop("0.5in")
							.setMarginBottom("0.5in").setMarginLeft("0.75in").setMarginRight("0.5in");

			// <fo:region-body region-name="xsl-region-body"
			// margin-bottom="0.6in"
			// />
			simplePageMaster.setVisibleRegionBody(true);
			FoRegionBody regionBody = simplePageMaster.getRegionBody();
			regionBody.setRegionName("xsl-region-body").setMarginBottom("25mm").setMarginTop("25mm");

			// <fo:region-after region-name="xsl-region-after" extent="0.5in"
			// precedence="true" />
			simplePageMaster.setVisibleRegionAfter(true);
			FoRegionAfter regionAfter = simplePageMaster.getRegionAfter();
			regionAfter.setRegionName("xsl-region-after").setExtent("15mm").setPrecedence("true");

			// <fo:region-before region-name="xsl-region-before" extent="0.5in"
			// precedence="true" />
			simplePageMaster.setVisibleRegionBefore(true);
			FoRegionBefore regionBefore = simplePageMaster.getRegionBefore();
			regionBefore.setRegionName("xsl-region-before").setExtent("30mm").setPrecedence("true");

			// <fo:region-start region-name="xsl-region-start" extent="0.5in" />
			simplePageMaster.setVisibleRegionStart(true);
			FoRegionStart regionStart = simplePageMaster.getRegionStart();
			regionStart.setRegionName("xsl-region-start").setExtent("0.5in");

			// <fo:region-end region-name="xsl-region-end" extent="0.5in" />
			simplePageMaster.setVisibleRegionEnd(true);
			FoRegionEnd regionEnd = simplePageMaster.getRegionEnd();
			regionEnd.setRegionName("xsl-region-end").setExtent("0.5in");

			// <fo:page-sequence master-reference="simple">
			FoPageSequence pageSequence = doc.getPageSequence();
			pageSequence.setMasterReference("simple");

			FoStaticContent beforeContent = new FoStaticContent();
			pageSequence.setBeforeContent(beforeContent);
			beforeContent.setFlowName("xsl-region-before").setDisplayAlign("before");

			{
				FoTable table = new FoTable().setTableLayout("fixed").setWidth("100%");
				beforeContent.add(table);
				table.add(new FoTableColumn().proportionalColumnWidth(10));
				table.add(new FoTableColumn().proportionalColumnWidth(38));

				FoTableBody body = new FoTableBody();
				table.add(body);
				{
					FoTableRow row = new FoTableRow();
					body.add(row);

					FoTableCell cell1 = new FoTableCell().setTextAlign("left").setNumberRowsSpanned("2");
					row.add(cell1);
					byte[] headerPicture = StringUtils.getResourceAsBytes("/sigla_gs1.jpg");
					File folder = new File(System.getProperty("java.io.tmpdir"));
					File file = new File(folder, System.currentTimeMillis() + "sigla_gs1.jpg");
					FileUtils.writeBytesToFile(headerPicture, file);
					cell1.add(new FoBlock().add(new FoExternalGraphic().setSrc(file.getPath())));
                    file.deleteOnExit();

					FoTableCell cell2 = new FoTableCell().setTextAlign("right");
					addMultilineText(cell2, headerText, null, null, "9pt");
					row.add(cell2);
				}
				{
					FoTableRow row = new FoTableRow();
					body.add(row);

					FoTableCell cell2 = new FoTableCell().setTextAlign("left");
					row.add(cell2);
					byte[] headerPicture = StringUtils.getResourceAsBytes("/hr2.gif");
					File folder = new File(System.getProperty("java.io.tmpdir"));
					File file = new File(folder, System.currentTimeMillis() + "hr2.gif");
					FileUtils.writeBytesToFile(headerPicture, file);
					cell2.add(new FoBlock().add(new FoExternalGraphic().setSrc(file.getPath())));
                    file.deleteOnExit();
                }
			}

			FoStaticContent afterContent = new FoStaticContent();
			pageSequence.setAfterContent(afterContent);
			afterContent.setFlowName("xsl-region-after").setDisplayAlign("after");

			{
				byte[] headerPicture = StringUtils.getResourceAsBytes("/hr2.gif");
				File folder = new File(System.getProperty("java.io.tmpdir"));
				File file = new File(folder, System.currentTimeMillis() + "hr2.gif");
				FileUtils.writeBytesToFile(headerPicture, file);
				afterContent.add(new FoBlock().add(new FoExternalGraphic().setSrc(file.getPath())));

				afterContent.add(newBlock(footerText).setTextAlign("center"));
                file.deleteOnExit();
            }

			pageContent = pageSequence.getPageContent();

		}
		return pageContent;
	}
}
