package ro.j4biz.juridice;

import ro.j4biz.commons.core.StringUtils;
import ro.j4biz.commons.poi.Excel2ObjectArray;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExcelParser {
	private final Object[][] excelAsMatrix;

	public ExcelParser(final byte[] bytes) throws Exception {
		List<Object[][]> sheets = new Excel2ObjectArray(bytes).getExcelAsMatrix();
		if (sheets.size() > 1) {
			throw new RuntimeException("Prea multe foi la excel. Trebuie doar una.");
		}
		if (sheets.size() == 0) {
			throw new RuntimeException("Excel gol. Trebuie doar o foaie.");
		}

		excelAsMatrix = sheets.get(0);
	}

	public List<CompanyBean> getLines() {
		List<CompanyBean> companies = new ArrayList<CompanyBean>();

		// atentie ca ciclam de la lina 1
		for (int i = 1; i < excelAsMatrix.length; i++) {
			Object[] row = excelAsMatrix[i];
			companies.add(parseRow(row, i));
		}

		return companies;
	}

	private CompanyBean parseRow(final Object[] row, final int rowNumber) {
		CompanyBean company = new CompanyBean();
		int column = -1;

		if (row.length <= 16) {
			throw new RuntimeException("Pe linia " + (rowNumber + 1) + " sunt prea putine coloane, trebuie sa fie 17");
		}

		//
		column++;
		company.setSocietateNume(getObligatoryString(row[column], rowNumber, column));
		//
		column++;
		company.setSocietateStradaNr(getObligatoryString(row[column], rowNumber, column));
		//
		column++;
		company.setSocietateCodPostal(getOptionalString(row[column], rowNumber, column));
		//
		column++;
		company.setSocietateLocalitate(getObligatoryString(row[column], rowNumber, column));
		//
		column++;
		company.setSocietateJudet(getObligatoryString(row[column], rowNumber, column));
		//
		column++;
		company.setSocietateCUI(getObligatoryString(row[column], rowNumber, column));
		//
		column++;
		company.setSocietateNrInregRegCom(getObligatoryString(row[column], rowNumber, column));

		//
		column++;
		company.setFacturaNr(getObligatoryString(row[column], rowNumber, column));
		//
		column++;
		company.setFacturaData(getObligatoryDate(row[column], rowNumber, column));
		//
		column++;
		company.setFacturaSumaDatorata(getObligatoryDouble(row[column], rowNumber, column));

		//
		column++;
		company.setContractNr(getObligatoryString(row[column], rowNumber, column));
		//
		column++;
		company.setContractData(getObligatoryDate(row[column], rowNumber, column));

		//
		column++;
		company.setAdresaInitialaNr(getOptionalString(row[column], rowNumber, column));
		//
		column++;
		company.setAdresaInitialaData(getOptionalDate(row[column], rowNumber, column));

		//
		column++;
		company.setAdresaNouaNr(getOptionalString(row[column], rowNumber, column));
		//
		column++;
		company.setAdresaNouaData(getOptionalDate(row[column], rowNumber, column));

		//
		column++;
		company.setAnulFacturat(getObligatoryString(row[column], rowNumber, column));

		if (row.length <= 16) {
			throw new RuntimeException("Pe linia " + (rowNumber + 1) + " sunt prea putine coloane, trebuie sa fie 17");
		}

		//
		column++;
		if (row.length > column) {
			company.setJudecatoria(getOptionalString(row[column], rowNumber, column));
		}

		//
		// column++;
		// if (row.length > column) {
		// company.setIban(getOptionalString(row[column], rowNumber, column));
		// }
		//
		// //
		// column++;
		// if (row.length > column) {
		// company.setBanca(getOptionalString(row[column], rowNumber, column));
		// }

		//
		column++;
		if (row.length > column) {
			company.setFacturaScadenta(getOptionalDate(row[column], rowNumber, column));
		}

		return company;
	}

	private String getObligatoryString(final Object object, final int rowNumber, final int column) {
		String errPrefix = "La linia [" + (rowNumber + 1) + "], coloana [" + (column + 1) + "] ";
		if (object == null) {
			throw new RuntimeException(errPrefix + "valoarea este nula.");
		}
		String value = object.toString();
		if (StringUtils.isEmpty(value)) {
			throw new RuntimeException(errPrefix + "celula este goala sau doar cu spatii.");
		}
		return value;
	}

	private Date getObligatoryDate(final Object object, final int rowNumber, final int column) {
		String errPrefix = "La linia [" + (rowNumber + 1) + "], coloana [" + (column + 1) + "] ";
		if (object == null) {
			throw new RuntimeException(errPrefix + "valoarea este nula.");
		}

		if (object instanceof Date) {
			return (Date) object;
		} else {
			throw new RuntimeException(errPrefix + "celula nu este de tip data.");
		}
	}

	private Double getObligatoryDouble(final Object object, final int rowNumber, final int column) {
		String errPrefix = "La linia [" + (rowNumber + 1) + "], coloana [" + (column + 1) + "] ";
		if (object == null) {
			throw new RuntimeException(errPrefix + "valoarea este nula.");
		}

		if (object instanceof Double) {
			return (Double) object;
		} else {
			try {
				return Double.parseDouble(object.toString());
			} catch (NumberFormatException exc) {
				throw new RuntimeException(errPrefix + "celula nu este de tip numeric: '" + object + "'");
			}
		}
	}

	private String getOptionalString(final Object object, final int rowNumber, final int column) {
		if (object == null) {
			return null;
		}
		String value = object.toString();
		if (StringUtils.isEmpty(value)) {
			return null;
		}
		return value;
	}

	private Date getOptionalDate(final Object object, final int rowNumber, final int column) {
		String errPrefix = "La linia [" + (rowNumber + 1) + "], coloana [" + (column + 1) + "] ";
		if (object == null) {
			return null;
		}

		if (object instanceof Date) {
			return (Date) object;
		} else {
			throw new RuntimeException(errPrefix + "celula nu este de tip data.");
		}
	}

}
