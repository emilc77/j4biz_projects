$(document).ready(function() {
    var containsTdsTheText = function($tds, token) {
        if (token === '') {
            return true;
        }

        var contained = false;
        $.each($tds, function() {
            var tdText = $( this ).text().toLowerCase();
            if (tdText.indexOf(token) > -1) {
                contained = true;
                return;
            }

            return;
        });
        return contained;
    }


    $("#search").keyup(function() {
        // alert("cuculeana");

        // hide all the rows
        $(".jsTr").hide();

        // split the current value of searchInput
        var data = this.value.toLowerCase().split(" ");

        //create a jquery object of the rows
        var jo = $(".jsTr");

        jo.filter(function () {
            var $currentTr = $(this);
            var $selectedTds = $currentTr.find(".jsTd");

            for (var d = 0; d < data.length; ++d) {
                if (! containsTdsTheText($selectedTds, data[d])) {
                    return false;
                }
            }

            return true;
        }).show(); //show the rows that match.


    }).focus(function(){
          this.value="";
          $(this).css({"color":"black"});
          $(this).unbind('focus');
    }).css({"color":"#C0C0C0"});

});
