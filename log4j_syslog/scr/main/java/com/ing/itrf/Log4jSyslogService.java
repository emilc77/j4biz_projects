package com.ing.itrf;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class Log4jSyslogService {
    private static final Logger loggerAuditTrail = LogManager.getLogger(AuditTrailBean.class);

    public void logSomething(AuditTrailBean auditTrail) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String message = mapper.writeValueAsString(auditTrail);
            loggerAuditTrail.info(message);
        } catch (JsonProcessingException e) {
            // TODO: to process the error
            // throw Throwables.propagate(e);
            throw new RuntimeException(e);
        }
    }


}
