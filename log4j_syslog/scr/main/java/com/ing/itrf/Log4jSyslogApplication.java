package com.ing.itrf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Log4jSyslogApplication {

    public static void main(String[] args) {
        SpringApplication.run(Log4jSyslogApplication.class, args);
    }
}
