package com.ing.itrf;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class AuditTrailBean {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSSZ")
    private Date date = new Date();

    private String username;

    private String pupilId;
    private String pupilFullName;
    private String pupilBirthday;
    private String pupilAddress;

    private String schoolClass;

    private String parentId;
    private String parentFullName;
    private String parentBirthday;
    private String parentAddress;
    private String parentJob;
    private String parentPhoneEmail;
    private String parentIban;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPupilId() {
        return pupilId;
    }

    public void setPupilId(String pupilId) {
        this.pupilId = pupilId;
    }

    public String getPupilFullName() {
        return pupilFullName;
    }

    public void setPupilFullName(String pupilFullName) {
        this.pupilFullName = pupilFullName;
    }

    public String getPupilBirthday() {
        return pupilBirthday;
    }

    public void setPupilBirthday(String pupilBirthday) {
        this.pupilBirthday = pupilBirthday;
    }

    public String getPupilAddress() {
        return pupilAddress;
    }

    public void setPupilAddress(String pupilAddress) {
        this.pupilAddress = pupilAddress;
    }

    public String getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(String schoolClass) {
        this.schoolClass = schoolClass;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getParentFullName() {
        return parentFullName;
    }

    public void setParentFullName(String parentFullName) {
        this.parentFullName = parentFullName;
    }

    public String getParentBirthday() {
        return parentBirthday;
    }

    public void setParentBirthday(String parentBirthday) {
        this.parentBirthday = parentBirthday;
    }

    public String getParentAddress() {
        return parentAddress;
    }

    public void setParentAddress(String parentAddress) {
        this.parentAddress = parentAddress;
    }

    public String getParentJob() {
        return parentJob;
    }

    public void setParentJob(String parentJob) {
        this.parentJob = parentJob;
    }

    public String getParentPhoneEmail() {
        return parentPhoneEmail;
    }

    public void setParentPhoneEmail(String parentPhoneEmail) {
        this.parentPhoneEmail = parentPhoneEmail;
    }

    public String getParentIban() {
        return parentIban;
    }

    public void setParentIban(String parentIban) {
        this.parentIban = parentIban;
    }
}
