package com.ing.itrf;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Log4jSyslogController {
    private static final Logger logger = LogManager.getLogger(Log4jSyslogController.class);

    @Autowired
    private Log4jSyslogService log4jSyslogService;

    /* it is POST because we will send passwords for this tester. */
    @RequestMapping(method = RequestMethod.GET, value = "/ping")
    public HttpEntity<String> ping(String username, String password, String factory, String queue) {
        logger.info("In Controller");
//        log4jSyslogService.logSomething("test");

        AuditTrailBean auditTrail = new AuditTrailBean();

        auditTrail.setUsername("username");

        auditTrail.setPupilId("pupil");
        auditTrail.setPupilFullName("PupilFullName");
        auditTrail.setPupilBirthday("PupilBirthday");
        auditTrail.setPupilAddress("PupilAddress");

        auditTrail.setSchoolClass("SchoolClass");

        auditTrail.setParentId("parentId");
        auditTrail.setParentFullName("parentFullName");
        auditTrail.setParentBirthday("ParentBirthday");
        auditTrail.setParentAddress("parentAddress");
        auditTrail.setParentJob("parentJob");
        auditTrail.setParentPhoneEmail("parentPhoneEmail");
        auditTrail.setParentIban("parentIban");

        log4jSyslogService.logSomething(auditTrail);

        if (true) {
            return new ResponseEntity<String>("", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
