package ro.j4biz.misc_java;

import java.io.File;

/**
 * Created by emil on 27.10.2016.
 */
public class PozeSamsungS4 {

    public static void renameFiles(File folder) {
        for (File file : folder.listFiles()) {
            String fileName = file.getName();
            System.out.print(fileName + " -> ");

            if (file.isDirectory()) {
                if (file.listFiles().length == 0) {
                    // empty directory
                    file.delete();
                }

                continue;
            }

            if (fileName.toUpperCase().startsWith("IMG_")) {
                // IMG_20160320_145258.jpg
                fileName = fileName.substring(4);
            }
            if (fileName.toUpperCase().startsWith("VID_")) {
                // VID_20160723_220729.mp4
                fileName = fileName.substring(4);
            }

            // 20150328_123715.jpg
            boolean isYyyyMmDd = fileName.matches("^[0-9]{6}.*$");
            boolean isYyyy_mm_dd = fileName.matches("^[0-9]{4}_[0-9]{2}_[0-9]{2}.*$");

            if (isYyyyMmDd || isYyyy_mm_dd) {
                if (isYyyy_mm_dd) {
                    // nothing to do.
                } else if (isYyyyMmDd) {
                    fileName = fileName.substring(0, 4) + "_"
                            + fileName.substring(4, 6) + "_"
                            + fileName.substring(6, 8) + fileName.substring(8);
                }

                File groupingFolder = new File(folder, fileName.substring(0, 10));
                groupingFolder.mkdir();

                File newFile = new File(groupingFolder, fileName);
                file.renameTo(newFile);

                System.out.print(fileName);
                System.out.println(" " + isYyyyMmDd + " - " + isYyyy_mm_dd + " ->");
                System.out.println(newFile);
            } else {
                System.err.println("Incorrect file: " + file.getName());
            }
        }


    }

    public static void main(String[] args) {
        File folder = new File("/home/emil/Downloads/poze_card");
        renameFiles(folder);
    }
}
