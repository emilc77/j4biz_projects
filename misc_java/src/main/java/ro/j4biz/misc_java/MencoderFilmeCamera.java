package ro.j4biz.misc_java;

import ro.j4biz.commons.core.StringUtils;

import java.util.List;

/**
 * Created by emil on 10.11.2016.
 */
public class MencoderFilmeCamera {

    private static void generateMencoderCommand(List<String> files) {
        for (String file : files) {
            if (! file.toLowerCase().endsWith(".avi")) {
                System.out.println("file does not end with .avi: " + file);
                continue;
            }

            System.out.println("mencoder -oac mp3lame -lameopts vbr=2:q=5:mode=3 -ovc xvid -xvidencopts vhq=4:fixed_quant=4 " + file + " -o " + file.substring(0, file.length() - 4) + "_2.avi");
        }
    }

    private static void generateDeleteCommand(List<String> files) {
        for (String file : files) {
            System.out.println("rm -f " + file);
        }
    }

    public static void main(String[] args) {
        //  find . | grep -i "\.avi" | grep -v "_2"
        String strFiles = StringUtils.getResourceAsStringISO_8859_1("/MencoderFilmeCamera.txt");
        List<String> files = StringUtils.splitIntoLines(strFiles);

        for (String file : files) {
            if (file.contains(" ")) {
                throw new RuntimeException("space in the file: '" + file + "'");
            }
        }

        generateMencoderCommand(files);
        System.out.println();
        System.out.println();
        generateDeleteCommand(files);
    }

}
