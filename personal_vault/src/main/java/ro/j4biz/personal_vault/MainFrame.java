package ro.j4biz.personal_vault;

import ro.j4biz.personal_vault.view.Frame;

public class MainFrame {
    public static void main(String[] args) {
        Frame frame = new Frame();
        frame.pack();
        frame.setVisible(true);

        frame.startUsingData();
    }
}
