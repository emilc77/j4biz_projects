package ro.j4biz.personal_vault.commons;

import com.google.common.base.Throwables;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import java.util.Base64;

public class Encryptor {
    private static final String AES_INIT_VECTOR = "Cuculeana Lugojeana Cuc";

    public static String encrypt(String key, String initVector, String value) {
        try {
            byte[] ivAsBytes = initVector.getBytes("UTF-8");
            ivAsBytes = prepareIv(ivAsBytes);
            IvParameterSpec iv = new IvParameterSpec(ivAsBytes);

            byte[] keyAsBytes = key.getBytes("UTF-8");
            keyAsBytes = prepareAesKey(keyAsBytes);
            SecretKeySpec skeySpec = new SecretKeySpec(keyAsBytes, "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());
            String base64Encrypted = Base64.getEncoder().encodeToString(encrypted);

            return base64Encrypted;
        } catch (Exception ex) {
            throw Throwables.propagate(ex);
        }

    }



    public static String decrypt(String key, String initVector, String encrypted) {
        try {
            byte[] ivAsBytes = initVector.getBytes("UTF-8");
            ivAsBytes = prepareIv(ivAsBytes);
            IvParameterSpec iv = new IvParameterSpec(ivAsBytes);

            byte[] keyAsBytes = key.getBytes("UTF-8");
            keyAsBytes = prepareAesKey(keyAsBytes);
            SecretKeySpec skeySpec = new SecretKeySpec(keyAsBytes, "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(Base64.getDecoder().decode(encrypted));

            return new String(original);
        } catch (Exception ex) {
            throw Throwables.propagate(ex);
        }
    }

    private static byte[] prepareIv(byte[] ivAsBytes) {
        byte[] newKey = new byte[16];

        if (ivAsBytes.length <= 16)  {
            System.arraycopy(ivAsBytes, 0, newKey, 0, ivAsBytes.length);
        } else {
            // taiem doar primele 16 de caractere.
            // TODO: sa cautam alta modalitate sa facem din ea ceva, un SHA etc.
            // java.security.InvalidAlgorithmParameterException: Wrong IV length: must be 16 bytes long
            System.arraycopy(ivAsBytes, 0, newKey, 0, 16);
        }

        return newKey;
    }


    // AES only supports key sizes of 16, 24 or 32 bytes.
    // You either need to provide exactly that amount or you derive the key from what you type in.
    private static byte[] prepareAesKey(byte[] keyAsBytes) {
        if (keyAsBytes.length <= 16)  {
            byte[] newKey = new byte[16];
            System.arraycopy(keyAsBytes, 0, newKey, 0, keyAsBytes.length);
            return newKey;

        } else if (keyAsBytes.length <= 24)  {
            byte[] newKey = new byte[24];
            System.arraycopy(keyAsBytes, 0, newKey, 0, keyAsBytes.length);
            return newKey;

        } else if (keyAsBytes.length <= 32)  {
            byte[] newKey = new byte[32];
            System.arraycopy(keyAsBytes, 0, newKey, 0, keyAsBytes.length);
            return newKey;

        } else {
            // taiem doar primele 32 de caractere.
            // TODO: sa cautam alta modalitate sa facem din ea ceva, un SHA etc.
            byte[] newKey = new byte[32];
            System.arraycopy(keyAsBytes, 0, newKey, 0, 32);
            return newKey;

        }
    }

    public static void main(String[] args) {
        String key = "Bar12345Bar12345"; // 128 bit key
        String initVector = "RandomInitVector"; // 16 bytes IV

        System.out.println(decrypt(key, initVector,
                encrypt(key, initVector, "Hello World")));
    }
}
