package ro.j4biz.personal_vault.commons;

import com.google.common.base.Throwables;
import com.google.common.io.ByteStreams;

import java.io.*;

public class IOUtils {

    public static String getResourceAsString(String resources) {
        byte[] result;
        try (InputStream resourceAsStream = IOUtils.class.getResourceAsStream(resources)) {
            if (resourceAsStream == null) {
                throw new RuntimeException("No resource in CLASSPATH with the path: '" + resources + "'");
            }
            result = ByteStreams.toByteArray(resourceAsStream);
            return new String(result, "UTF-8");
        } catch (IOException e) {
            throw Throwables.propagate(e);
        }
    }

    public static String readFileAsString(File file) {
        byte[] result;
        try (FileInputStream resourceAsStream = new FileInputStream(file)) {
            result = ByteStreams.toByteArray(resourceAsStream);
            return new String(result, "UTF-8");
        } catch (IOException e) {
            throw Throwables.propagate(e);
        }
    }

    public static void writeFile(File file, byte[] content) {
        try {
            try (FileOutputStream fos = new FileOutputStream(file)) {
                fos.write(content);
            }
        } catch (IOException e) {
            throw Throwables.propagate(e);
        }
    }

    public static void writeFile(File file, String content) {
        try {
            byte[] contentAsBytes = content.getBytes("UTF-8");
            try (FileOutputStream fos = new FileOutputStream(file)) {
                fos.write(contentAsBytes);
            }
        } catch (IOException e) {
            throw Throwables.propagate(e);
        }
    }

}
