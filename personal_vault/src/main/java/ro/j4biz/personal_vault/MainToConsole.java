package ro.j4biz.personal_vault;

import ro.j4biz.personal_vault.dao.VaultDao;
import ro.j4biz.personal_vault.dao.file_storage.VaultFileDaoImpl;
import ro.j4biz.personal_vault.model.Entry;

import javax.swing.*;
import java.util.List;

public class MainToConsole {
    public static void main(String[] args) {
        VaultDao dao = new VaultFileDaoImpl();

        String vaultPassword = readPassword();
        if (vaultPassword == null) {
            return;
        }

        printAll(dao, vaultPassword);

        // adauga aici daca ai una noua.
        // add(dao, vaultPassword, "ra52tk", "ra52tk", "", ".");

        // edit(dao, vaultPassword, 3, "ra52tk", "ra52tk", "", ".");
    }

    private static String readPassword() {
        return JOptionPane.showInputDialog(null, "Enter password");
    }

    private static void printAll(VaultDao dao, String vaultPassword) {
        List<Entry> allDecoded = dao.getAllDecoded(vaultPassword);
        for (Entry entry : allDecoded) {
            System.out.println(" ---- " + entry.getLabel() + " ---- (" + entry.getId() + ")");
            System.out.println("username='" + entry.getUsername() + "'");
            System.out.println("password='" + entry.getPassword() + "'");
            System.out.println("details=" + entry.getDetails());
            System.out.println();
        }
    }


    private static void add(VaultDao dao, String vaultPassword, String label, String username, String password, String details) {
        Entry entry = new Entry();
        entry.setLabel(label);
        entry.setUsername(username);
        entry.setPassword(password);
        entry.setDetails(details);

        dao.add(vaultPassword, entry);
    }

    private static void edit(VaultDao dao, String vaultPassword, Integer id, String label, String username, String password, String details) {
        Entry entry = new Entry();
        entry.setId(id);
        entry.setLabel(label);
        entry.setUsername(username);
        entry.setPassword(password);
        entry.setDetails(details);

        dao.edit(vaultPassword, entry);
    }
}
