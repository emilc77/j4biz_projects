package ro.j4biz.personal_vault.view;

import ro.j4biz.personal_vault.model.Entry;

import javax.swing.table.AbstractTableModel;
import java.text.SimpleDateFormat;

public class TableModel extends AbstractTableModel {
    private final Model model;
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public TableModel(Model model) {
        this.model = model;
    }

    @Override
    public int getRowCount() {
        return model.getEntries().size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    public String getColumnName(int columnIndex) {
        if (columnIndex == 0) {
            return "ID";
        } if (columnIndex == 1) {
            return "Label";
        } if (columnIndex == 2) {
            return "Username";
        } if (columnIndex == 3) {
            return "Created";
        } if (columnIndex == 4) {
            return "Last Modified";
        } else {
            throw new RuntimeException("Unaccepted column index: " + columnIndex);
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Entry entry = model.getEntries().get(rowIndex);
        if (columnIndex == 0) {
            return entry.getId();
        } if (columnIndex == 1) {
            return entry.getLabel();
        } if (columnIndex == 2) {
            return entry.getUsername();
        } if (columnIndex == 3) {
            return SDF.format(entry.getCreated());
        } if (columnIndex == 4) {
            return SDF.format(entry.getLastModified());
        } else {
            throw new RuntimeException("Unaccepted column index: " + columnIndex);
        }
    }

}
