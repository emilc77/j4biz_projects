package ro.j4biz.personal_vault.view;

import ro.j4biz.personal_vault.model.Entry;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Frame extends JFrame {
    private Model model = new Model();

    private JPanel panelMain;

    private JTable table;
    private TableModel tableModel;

    private JTextField textFieldId;
    private JTextField textFieldLabel;
    private JTextField textFieldUsername;
    private JTextField textFieldPassword;
    private JTextArea textareaDetails;

    private JPanel panelButtons;
    private JButton buttonAdd;
    private JButton buttonEdit;

    public Frame() {
        super("Passwords");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        getContentPane().add(getPanelMain(), BorderLayout.CENTER);
    }

    public JPanel getPanelMain() {
        if (panelMain == null) {
            panelMain = new JPanel();

            panelMain.setLayout(new GridBagLayout());

            // --------- line ------------
            int line = 0;
            {
                GridBagConstraints c = new GridBagConstraints();
                c.gridx = 0;
                c.gridy = line;
                c.gridwidth = 2;
                c.gridheight = 1;
                c.fill = GridBagConstraints.BOTH;
                c.weightx = 1;
                c.weighty = 1;

                JScrollPane scrollPane = new JScrollPane(getTable());
                getTable().setFillsViewportHeight(true);
                panelMain.add(scrollPane, c);
            }

            // --------- line ------------
            line++;
            {
                GridBagConstraints c = new GridBagConstraints();
                c.gridx = 0;
                c.gridy = line;
                panelMain.add(new JLabel("Id:"), c);
            }

            {
                GridBagConstraints c = new GridBagConstraints();
                c.gridx = 1;
                c.gridy = line;
                c.anchor = GridBagConstraints.WEST;
                panelMain.add(getTextFieldId(), c);
            }

            // --------- line ------------
            line++;
            {
                GridBagConstraints c = new GridBagConstraints();
                c.gridx = 0;
                c.gridy = line;
                panelMain.add(new JLabel("Label:"), c);
            }

            {
                GridBagConstraints c = new GridBagConstraints();
                c.gridx = 1;
                c.gridy = line;
                c.anchor = GridBagConstraints.WEST;
                panelMain.add(getTextFieldLabel(), c);
            }

            // --------- line ------------
            line++;
            {
                GridBagConstraints c = new GridBagConstraints();
                c.gridx = 0;
                c.gridy = line;
                panelMain.add(new JLabel("Username:"), c);
            }

            {
                GridBagConstraints c = new GridBagConstraints();
                c.gridx = 1;
                c.gridy = line;
                c.anchor = GridBagConstraints.WEST;
                panelMain.add(getTextFieldUsername(), c);
            }


            // --------- line ------------
            line++;
            {
                GridBagConstraints c = new GridBagConstraints();
                c.gridx = 0;
                c.gridy = line;
                c.fill = GridBagConstraints.BOTH;
                panelMain.add(new JLabel("Password:"), c);
            }

            {
                GridBagConstraints c = new GridBagConstraints();
                c.gridx = 1;
                c.gridy = line;
                c.anchor = GridBagConstraints.WEST;
                panelMain.add(getTextFieldPassword(), c);
            }

            // --------- line ------------
            line++;
            {
                GridBagConstraints c = new GridBagConstraints();
                c.gridx = 0;
                c.gridy = line;
                panelMain.add(new JLabel("Description:"), c);
            }

            {
                GridBagConstraints c = new GridBagConstraints();
                c.gridx = 1;
                c.gridy = line;
                c.fill = GridBagConstraints.BOTH;
                c.weightx = 1;
                c.weighty = 2.0;

                JScrollPane scrollPane = new JScrollPane(getTextareaDetails());
                panelMain.add(scrollPane, c);
            }


            // --------- line ------------
            line++;
            {
                GridBagConstraints c = new GridBagConstraints();
                c.gridx = 0;
                c.gridy = line;
                c.gridwidth = 2;
                c.gridheight = 1;
                c.fill = GridBagConstraints.BOTH;
                c.weightx = 1;
                c.weighty = 1;

                panelMain.add(getPanelButtons(), c);
            }



        }
        return panelMain;
    }


    public JTable getTable() {
        if (table == null) {
            table = new JTable();
            table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            table.setModel(getTableModel());
            table.getSelectionModel().addListSelectionListener(
                    new ListSelectionListener() {
                        public void valueChanged(ListSelectionEvent event) {
                            int viewRow = table.getSelectedRow();
                            if (viewRow < 0) {
                                getButtonAdd().setEnabled(true);
                                getButtonEdit().setEnabled(false);

                                //Selection got filtered away.
                                clearTextInAllFields();

                            } else {
                                getButtonAdd().setEnabled(false);
                                getButtonEdit().setEnabled(true);

                                int modelRow = table.convertRowIndexToModel(viewRow);
                                Entry entityByIndex = model.getEntries().get(modelRow);

                                getTextFieldId().setText("" + entityByIndex.getId());
                                getTextFieldLabel().setText("" + entityByIndex.getLabel());
                                getTextFieldUsername().setText("" + entityByIndex.getUsername());
                                getTextFieldPassword().setText("" + entityByIndex.getPassword());
                                getTextareaDetails().setText("" + entityByIndex.getDetails());
                            }
                        }
                    }
            );
        }
        return table;
    }

    private void clearTextInAllFields() {
        getTextFieldId().setText("");
        getTextFieldLabel().setText("");
        getTextFieldUsername().setText("");
        getTextFieldPassword().setText("");
        getTextareaDetails().setText("");
    }

    public TableModel getTableModel() {
        if (tableModel == null) {
            tableModel = new TableModel(model);
        }
        return tableModel;
    }

    public JTextField getTextFieldId() {
        if (textFieldId == null) {
            textFieldId = new JTextField();
            textFieldId.setEnabled(false);
            textFieldId.setColumns(5);
        }
        return textFieldId;
    }

    public JTextField getTextFieldLabel() {
        if (textFieldLabel == null) {
            textFieldLabel = new JTextField();
            textFieldLabel.setColumns(50);
        }
        return textFieldLabel;
    }

    public JTextField getTextFieldUsername() {
        if (textFieldUsername == null) {
            textFieldUsername = new JTextField();
            textFieldUsername.setColumns(50);
        }
        return textFieldUsername;
    }

    public JTextField getTextFieldPassword() {
        if (textFieldPassword == null) {
            textFieldPassword = new JTextField();
            textFieldPassword.setColumns(50);
        }
        return textFieldPassword;
    }

    public JTextArea getTextareaDetails() {
        if (textareaDetails == null) {
            textareaDetails = new JTextArea(10, 100);
        }
        return textareaDetails;
    }

    public JPanel getPanelButtons() {
        if (panelButtons == null) {
            panelButtons = new JPanel();

            panelButtons.setLayout(new GridBagLayout());

            // --------- line ------------
            {
                GridBagConstraints c = new GridBagConstraints();
                c.gridx = 0;
                c.gridy = 0;
                panelButtons.add(getButtonAdd(), c);
            }

            {
                GridBagConstraints c = new GridBagConstraints();
                c.gridx = 1;
                c.gridy = 0;
                panelButtons.add(getButtonEdit(), c);
            }

        }
        return panelButtons;
    }

    public JButton getButtonAdd() {
        if (buttonAdd == null) {
            buttonAdd = new JButton("ADD");
            buttonAdd.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Entry entry = new Entry();
                    entry.setLabel(getTextFieldLabel().getText());
                    entry.setUsername(getTextFieldUsername().getText());
                    entry.setPassword(getTextFieldPassword().getText());
                    entry.setDetails(getTextareaDetails().getText());

                    model.getDao().add(model.getVaultPassword(), entry);

                    model.reload();
                    getTableModel().fireTableDataChanged();

                    getTable().clearSelection();

                    // la add nu se trigareste curatenia.
                    clearTextInAllFields();
                }
            });
        }
        return buttonAdd;
    }

    public JButton getButtonEdit() {
        if (buttonEdit == null) {
            buttonEdit = new JButton("EDIT");
            buttonEdit.setEnabled(false);

            buttonEdit.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Entry entry = new Entry();
                    entry.setId(Integer.valueOf(getTextFieldId().getText()));
                    entry.setLabel(getTextFieldLabel().getText());
                    entry.setUsername(getTextFieldUsername().getText());
                    entry.setPassword(getTextFieldPassword().getText());
                    entry.setDetails(getTextareaDetails().getText());

                    model.getDao().edit(model.getVaultPassword(), entry);

                    model.reload();
                    getTableModel().fireTableDataChanged();

                    getTable().clearSelection();
                }
            });

        }
        return buttonEdit;
    }

    public void startUsingData() {
        String vaultPassword = readPassword();
        if (vaultPassword == null) {
            System.exit(-1);
            //return;
        }
        model.setVaultPassword(vaultPassword);

        // to validate the password only.
        if (! model.isPasswordOk()) {
            JOptionPane.showMessageDialog(this, "ERROR when reading the file, most probably the password is not correct.");
            System.exit(-1);
        }

        model.reload();
        getTableModel().fireTableDataChanged();
    }

    private String readPassword() {
//        JPanel panel = new JPanel();
//        JLabel label = new JLabel("Enter a password:");
//        JPasswordField pass = new JPasswordField(40);
//        panel.add(label);
//        panel.add(pass);
//        String[] options = new String[]{"OK", "Cancel"};
//        int option = JOptionPane.showOptionDialog(null, panel, "The title",
//                JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
//                null, options, options[1]);
//        if(option == 0) {
//            // pressing OK button
//            char[] password = pass.getPassword();
//            return new String(password);
//        } else {
//            return null;
//        }


        JPasswordField pf = new JPasswordField();
        int okCxl = JOptionPane.showConfirmDialog(null, pf, "Enter Password", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

        if (okCxl == JOptionPane.OK_OPTION) {
            String password = new String(pf.getPassword());
            return password;
        } else {
            return null;
        }

        // return JOptionPane.showInputDialog(this, "Enter password");
    }

}
