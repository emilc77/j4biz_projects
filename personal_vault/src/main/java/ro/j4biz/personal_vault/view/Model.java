package ro.j4biz.personal_vault.view;

import ro.j4biz.personal_vault.dao.VaultDao;
import ro.j4biz.personal_vault.dao.file_storage.VaultFileDaoImpl;
import ro.j4biz.personal_vault.model.Entry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Model {
    private final VaultDao dao = new VaultFileDaoImpl();
    private String vaultPassword;
    private List<Entry> allDecoded;

    public VaultDao getDao() {
        return dao;
    }

    public String getVaultPassword() {
        return vaultPassword;
    }

    public void setVaultPassword(String vaultPassword) {
        this.vaultPassword = vaultPassword;
    }

    public List<Entry> getEntries() {
        if (allDecoded == null) {
            if (vaultPassword == null) {
                allDecoded = new ArrayList<>();
            } else {
                allDecoded = Collections.unmodifiableList(dao.getAllDecoded(vaultPassword));
            }
        }
        return allDecoded;
    }

    public void reload() {
        allDecoded = null;
    }

    public boolean isPasswordOk() {
        try {
            dao.getAllDecoded(vaultPassword);
            return true;
        } catch (RuntimeException exc) {
            return false;
        }
    }
}
