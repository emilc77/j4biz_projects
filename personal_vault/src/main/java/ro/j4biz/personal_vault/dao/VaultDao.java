package ro.j4biz.personal_vault.dao;

import ro.j4biz.personal_vault.model.Entry;

import java.util.List;

public interface VaultDao {
    List<Entry> getAllDecoded(String password);
    void add(String password, Entry entry);
    void edit(String password, Entry entry);
}
