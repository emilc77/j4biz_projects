package ro.j4biz.personal_vault.dao.file_storage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.base.Throwables;
import ro.j4biz.personal_vault.commons.Encryptor;
import ro.j4biz.personal_vault.commons.IOUtils;
import ro.j4biz.personal_vault.dao.VaultDao;
import ro.j4biz.personal_vault.model.Entry;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VaultFileDaoImpl implements VaultDao {
    private static final String AES_INIT_VECTOR = "Cuculeana Lugojeana Cuc";

    private File getFileStorage() {
        String strFile = System.getProperty("FILE_STORAGE");
        if (strFile == null || strFile.length() == 0) {
            System.err.println("Null or empty property 'FILE_STORAGE'");
            System.exit(-1);
        }
        File file = new File(strFile);
        return file;
    }

    @Override
    public List<Entry> getAllDecoded(String password) {
        if (!getFileStorage().exists()) {
            // first execution
            writeFileContent(new FileContent());
        }

        FileContent fileContent = readFileContent();

        for (Entry entry : fileContent.getEntries()) {
            entry.setLabel(decrypt(password, entry.getLabel()));
            entry.setUsername(decrypt(password, entry.getUsername()));
            entry.setPassword(decrypt(password, entry.getPassword()));
            entry.setDetails(decrypt(password, entry.getDetails()));
        }
        return fileContent.getEntries();
    }

    @Override
    public void add(String password, Entry entry) {
        FileContent fileContent = readFileContent();

        Entry encryptedEntry = encryptEntity(password, entry, fileContent);
        encryptedEntry.setId(fileContent.retriveSmallestUnusedId());
        Date now = new Date();
        encryptedEntry.setCreated(now);
        encryptedEntry.setLastModified(now);

        fileContent.getEntries().add(encryptedEntry);
        writeFileContent(fileContent);
    }


    @Override
    public void edit(String password, Entry entry) {
        FileContent fileContent = readFileContent();

        Entry encryptedEntry = encryptEntity(password, entry, fileContent);
        encryptedEntry.setLastModified(new Date());

        fileContent.replace(encryptedEntry);
        writeFileContent(fileContent);
    }

    private Entry encryptEntity(String password, Entry entry, FileContent fileContent) {
        Entry encryptedEntry = new Entry();
        encryptedEntry.setId(entry.getId());
        encryptedEntry.setLabel(encrypt(password, entry.getLabel()));
        encryptedEntry.setUsername(encrypt(password, entry.getUsername()));
        encryptedEntry.setPassword(encrypt(password, entry.getPassword()));
        encryptedEntry.setDetails(encrypt(password, entry.getDetails()));
        return encryptedEntry;
    }

    private static String encrypt(String password, String value) {
        return Encryptor.encrypt(password, AES_INIT_VECTOR, value);
    }

    private static String decrypt(String password, String encryptedValue) {
        return Encryptor.decrypt(password, AES_INIT_VECTOR, encryptedValue);
    }

    private FileContent readFileContent() {
        try {
            String resourceJson = IOUtils.readFileAsString(getFileStorage());
            ObjectMapper mapper = new ObjectMapper();
            FileContent fileContent = mapper.readValue(resourceJson, FileContent.class);

            // TODO: to remove in the future:
            for (Entry entry : fileContent.getEntries()) {
                if (entry.getCreated() == null) {
                    entry.setCreated(new Date());
                }
                if (entry.getLastModified() == null) {
                    entry.setLastModified(new Date());
                }
            }

            return fileContent;
        } catch (IOException e) {
            throw Throwables.propagate(e);
        }
    }

    private void writeFileContent(FileContent fileContent) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            byte[] bytes = mapper.writeValueAsBytes(fileContent);
            IOUtils.writeFile(getFileStorage(), bytes);
        } catch (JsonProcessingException e) {
            throw Throwables.propagate(e);
        }
    }

}
