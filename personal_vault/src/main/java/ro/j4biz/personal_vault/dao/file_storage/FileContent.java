package ro.j4biz.personal_vault.dao.file_storage;

import ro.j4biz.personal_vault.model.Entry;

import java.util.ArrayList;
import java.util.List;

public class FileContent {
    private List<Entry> entries = new ArrayList<>();

    public int retriveSmallestUnusedId(){
        int max = 0;
        for (Entry entry : entries) {
            max = Math.max(max, entry.getId().intValue());
        }
        return max + 1;
    }

    public void replace(Entry edited) {
        for (Entry entry : entries) {
            if (entry.getId().equals(edited.getId())) {
                entry.setLabel(edited.getLabel());
                entry.setUsername(edited.getUsername());
                entry.setPassword(edited.getPassword());
                entry.setDetails(edited.getDetails());
            }
        }
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

}
